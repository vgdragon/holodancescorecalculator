This files is a documentation for the replay data files that you will usually 
find in GameData/PlaySessions. These files will not be created if you have 
disabled recording of gameplay data in the performance settings. You can define
the path where these files are stored in Configuration.json.

The files are stored compressed via gzip by default. You can use e.g. 7zip to
decompress them and then view them in any text editor. But they are big, so watch
out. Also, they are stored without formatting. Visual Studio, for example, will
let you format those easily. However, there are more useful when processed via
a JSON parser in a program you could write to analyze this data.

The meta-data stored in PlaySessions.json and also in this file is documented
in PlaySessionJSON-README.txt


Rough algorithm for calculating the score (not taking into account velocity orbs):

Note: baseScore for Sliders and Spinners already has the percentage of time spent
in the slider included:


baseScore = (int)(((float)sliderScore) * accumulatedTime / note.durationTime);

rawScoreWithBonus = baseScore +
                 (  actualBonusHead // usually 0, unless caught with head
                  + actualBonusFeet // usually 0, unless caught with feet
                  + caughtWithDifferentBonus // see below
                  + actualVelocityBonusScore // see below
                  + actualRotationBonusScore // see below
                  + actualBonusSlider        // see below (always 0 if not a Slider)
                  + actualBonusSpinner       // see below (always 0 if not a Spinner)
                  + actualBonusOrbDistance   // see below (always 0 if not an Orb)
				) * 

Then, for orbs, timing (normalizedFactor) is applied to get: timedScoreWithBonus (see below)

Then, currentMultiplier is applied to get: scoreWithMultiplier

Finally, balanceMultiplier (which is currently always 1.0) is applied to get: finalScore

This is then added to get totalScore which is the sum of all scores in that session so far.

To get the bonus values with the multipliers applied (e.g. for a graph of what percentage
they take in the finalScore), multiply them with normalizedFactor * balanceMultiplier * currentMultiplier, e.g.:

actualVelocityBonusScore * normalizedFactor * balanceMultiplier * currentMultiplier;



version
Version of this file. Currently 1; will be incremented every time we change the
format.

gameVersion
Version of Holodance that this file was created with.

playedDate
Date when this session was completed. This is basically DateTime.Now.Ticks:
https://msdn.microsoft.com/en-us/library/system.datetime.ticks(v=vs.110).aspx
This value is also used as filename for the session.

playerSelf
Information about the player that has recorded this session.

leftHandSteps, rightHandSteps, headSteps, movePlayerAreaSteps
This is the movement data of left hand, right hand and head, as well as of the
"play area" which can move and rotate during gameplay based on where the orb
source is. Each entry has a timestamp (seconds), position (Vector3) and
rotation (Quaternion).

recordedEvents
This is a list of all gameplay events.

eventIndex
The id of the event in the beatmap. The first "flying note" in the beatmap has 0,
the second 1 and so forth. This can be used to match the data from the beatmap
with the data stored here.

touchType
What kind of "flying note" is this? 
	public enum TouchType {
		Orb = 0,
		Arc = 1, // aka "Slider"
		Random = 2, // this mechanic is currently not supported
		Spinner = 3, // added to have full osu! support
	}

pickupType
How should this orb be picked up? Currently, this will usually be 0 because
Holodance does not tell you how to catch the orbs. One exception is in Story
Mode, Level 3, Tracks 3 and 4 where we have orbs that are meant to be caught
with your head.
    public enum PickupType {
        Any = 0, // catch it however you like
        Left = 1, // catch it with your left hand
        Right = 2, // catch it with your right hand
        Head = 3, // catch it with your head (those have a lower base score)
        LeftFoot = 4, // when you have Vive trackers ...
        RightFoot = 5 // when you have Vive trackers ...
    }

platformPositionAtInstantiation, platformRotationAtInstantiation
This is to have the platform offset which can be different in different
environments, and also for different players in multiplayer.

instantiateTime
The time when the "flying note" is created and first visible to the player.
This is perfectCatchTime - (bufferAheadQuarters * timePerQuarter).
bufferAheadQuarters is stored in PlaySession.json (also in this file),
timePerQuarter depends on the current BPM at the time of generation of
the orb. This is seconds into the audio-file of the song. See also flyDuration.

instantiatePosition
Where is the "flying note" created. This depends on the flying note source.
In the case of dragons (Story Mode), the flying note source can have quite
a bit of movement. This position is in world space, so it depends on the
environment.

perfectCatchTime
When is the perfect time to catch this flying note. In the case of Sliders and
Spinners it is the start time. This is seconds into the audio-file of the song.

perfectCatchPosition
Where is the perfect location to catch this orb. In cases of Sliders it's the
reference point where the Slider beings. With Spinners, it's the center of the
Spinner. This position is in world space, so it depends on the environment.

noteDuration
Duration of the note, only used for Sliders and Spinners.

flyDuration
How long is the "flying note" flying from instantiatePosition to 
perfectCatchPosition. This should be perfectCatchTime - instantiateTime.

showTimingDuration
How long do you see the timing indicators (timing lasers, timing circles), 
ahead of perfectCatchTime.

repeatCount
For Sliders, how often is the Slider repeated?

baseScore
The base score of this flying note. This is the score you will receive before
any bonuses are added, when the timing is perfect. We have different base score
values for orbs than for sliders and spinners.

midiVelocityOrb
Is this an orb that uses MIDI velocity (how strong a key is hit) for scaling
both the size of the orb and its score?

midiVelocityAddScore
The score added to baseScore, if midiVelocityOrb is true.

normalizedMidiVelocity
MIDI velocities can have integer values between 0 and 127. This is the velocity
normalized to a value between 0 and 1 using the following formula:
normalizedVelocity = 0.3F + 0.7F * ((float)note.velocity) / 127F;

baseScoreForMidiVelocityOrb
A value calculated using the following formula:
score = baseScore;
baseScoreForMidiVelocityOrb = (int)((score + velocityAddScore) * normalizedVelocity);

bonusOrbVelocity
The maximum possible bonus you can get for this orb based on the maximum 
velocity of the controller before the orb is caught.

bonusOrbRotation
The maximum possible bonus you can get for this orb based on the maximum 
rotational velocity of the controller before the orb is caught.

bonusHead
Bonus you will get added when you catch this orb with your head.

bonusFeet
Bonus you will get added when you catch this orb with your feet.

headOfftimeMultiplier
As catching the orb perfectly in time is harder when you use your head to catch
the orb, the off-time (how many seconds too early or too late) is multiplied
with this value.

bonusOrbDistance
The maximum possible bonus you can achieve for this orb based on the distance
the controller you caught this orb with has travelled since the last orb you
caught with this controller. This distance is only counted when you have not
missed the previous orb. The actual bonus achieved for this orb is stored in:
actualBonusOrbDistance. This bonus is there so that jumps (two orbs in sequence
having very different locations), which are much more difficult to handle than 
streams (multiple orbs arriving at almost the same location), give you the
extra score that you deserve. It will also increase your score when you move
between catching orbs, when you can.

bonusSlider
The maximum possible bonus you can achieve for this Slider based on the
distance covered while following the Slider, in relation to the duration of
the Slider. See also actualBonusSlider.

bonusSpinner
The maximum possible bonus you can achieve for this Spinner based on the
distance covered while following the Spinner, in relation to the duration of
the Spinner. See also actualBonusSpinner.

caught
Was this orb caught, or the slider/spinner handled successfully?

timingOffset
The actual time between perfectCatchTime and catchTime. Only is counted for
orbs.

timingOffsetWithHeadTolerance
This is the timingOffset multiplied by headOfftimeMultiplier.

caughtWith
Which was the first tracked object (controllers, head, feet) that touched the
orb/slider/spinner? See also touchType (it's the same values):
	public enum TouchType {
		Orb = 0,
		Arc = 1, // aka "Slider"
		Random = 2, // this mechanic is currently not supported
		Spinner = 3, // added to have full osu! support
	}

caughtWithDifferent
How many times did you use a different tracked object to catch the item. If
you caught the previous orb with your left hand, and this orb also with your
left hand, this will be 0. If you caught this with your left hand after 
catching the previous orbs with your left hand, then right hand, left foot, 
right foot and head, this should be 4.

caughtWithDifferentBonus
The bonus you receive based on caughtWithDifferent. This gives you 50 (at 1),
100, 200 and 400.

catchTime
The time when the orb was caught.

catchPosition
The position where the orb was caught.

comboCount
How many flying notes were caught without missing one?

currentMultiplier
Current combo multiplier.

rawScoreWithBonus
The score with all bonuses added without the timing multiplier applied.
This is the score you would receive if the timing was perfect.
IMPORTANT: For sliders and spinners, the percentage of time that you
spent handling the slider is already included in this. See also:
accumulatedTime and noteDuration.

normalizedFactor
This one is new since 0.8.0c12, version: 1 - it's the factor applied to 
rawScoreWithBonus to get timedScoreWithBonus

timedScoreWithBonus
Raw score with a normalized timing factor applied. The offTimePenaltyCurve
is a curve from 0 to 1 that increases slowly at first and quicker at the end,
so that a little offTime won't hurt much but longer offTimes will hurt more.
maxAllowedOffTime is currently set to 0.08. offTime is Mathf.Abs(timingOffset).

    float normalizedFactor = 1F - offTimePenaltyCurve.Evaluate(offTime / maxAllowedOffTime);
    timedScoreWithBonus = (int)(((float)scoreOfNote) * normalizedFactor);

actualBonusHead
The value from bonusHead, if the orb was caught with your head. 
This is added to baseScore to become part of rawScoreWithBonus.

actualBonusFeet
The value from bonusFeet, if the orb was caught with your feet.
This is added to baseScore to become part of rawScoreWithBonus.

velocityBonusFactor
A value between 0 and 1 based on the maximum velocity of the controller before 
the orb was caught. 1 means "maximum velocity", 0 means you just held the
controller in location without moving it.

actualVelocityBonusScore
This is added to baseScore to become part of rawScoreWithBonus:
actualVelocityBonusScore = (int) (velocityBonusFactor * bonusOrbVelocity);

rotationBonusFactor
A value between 0 and 1 based on the maximum rotational velocity of the 
controller before the orb was caught. 1 means "maximum velocity", 0 means 
you just held the controller in location without moving it.

actualRotationBonusScore
This is added to baseScore to become part of rawScoreWithBonus:
actualRotationBonusScore = (int) (rotationBonusFactor * bonusOrbRotation);

distance
For orbs, this is the distance in meters between catching the last "flying note"
with this controller, and catching this "flying note" with this controller, if
no orb was missed between those two.
For Sliders and Spinners, this is the distance in meters covered while 
successfully "sliding" or "spinning".

distanceMultiplier
distance run through a curve that increases quickly at first and more slowly
later, so that small increases in distance will have significant impact at
overall small distance but less impact when we already have a larger distance.
We have different curves here for orbs and sliders/spinners because there
are different methods of calculating those distances (see distance).
are different methods of calculating those distances (see distance).

distanceByTimeMultiplier
For Sliders:
distanceByTimeMultiplier = Mathf.Clamp01(distanceMultiplier / noteDuration);
For Spinners:
distanceByTimeMultiplier = Mathf.Clamp01(distanceMultiplier / Mathf.Clamp(noteDuration, 0.1F, 2F));

actualBonusOrbDistance
This is added to baseScore to become part of rawScoreWithBonus:
actualBonusOrbDistance = (int)(distanceMultiplier * bonusOrbDistance);

actualBonusSlider
This is added to baseScore to become part of rawScoreWithBonus:
actualBonusSlider = (int) (distanceByTimeMultiplier * bonusSlider);

actualBonusSpinner
actualBonusSpinner = (int) (distanceByTimeMultiplier * bonusSpinner);

accumulatedTime
Time in seconds spent in a Slider or Spinner.
accumulatedTime / noteDuration gives you the percentage of time spent in the 
slider or spinner, which is used to calculate the baseScore.

scoreWithMultiplier
scoreWithMultiplier = timedScoreWithBonus * currentMultiplier;

balanceMultiplier
A multiplier that could be applied to scoreWithMultiplier to balance the maximum
total score. This is currently set to 1.0 and will probably only ever be used
in Store Mode. We could, however, also use this for special game modes (e.g.
hidden).

finalScore
That is the final score you receive from this orb, with all bonuses and 
multipliers applied.

totalScore
The sum of all finalScores up to this point in the session.








Example:

{
  "version": 0, // this is from the PlaySession
  "gameVersion": "0.8.0c12", // this is from the PlaySession
  [...]
  "replayData": [
    {
      "version": 0,
      "gameVersion": "0.8.0c12",
      "playedDate": 636395179507594794,
      "playerSelf": {
        "version": 0,
        "playerPlatforms": 1,
        "steamID": 76561198091442463,
        "nickname": "jashan",
        "playerHeight": 1.8705071210861207,
        "playerChestHeight": 1.399999976158142
      },
      "recordedEvents": [
// example for a slider:
        {
          "eventIndex": 0,
          "touchType": 1,
          "pickupType": 0,
          "platformPositionAtInstantiation": {
            "x": 0.0,
            "y": 0.25,
            "z": -2.0
          },
          "platformRotationAtInstantiation": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0,
            "w": 1.0
          },
          "instantiateTime": 8.405691146850586,
          "instantiatePosition": {
            "x": -0.5078125,
            "y": 2.9453125,
            "z": 13.0
          },
          "perfectCatchTime": 9.589000701904297,
          "perfectCatchPosition": {
            "x": -0.20230330526828767,
            "y": 1.8836393356323243,
            "z": -1.3598828315734864
          },
          "noteDuration": 0.2999100387096405,
          "flyDuration": 1.183309555053711,
          "showTimingDuration": 0.4510999917984009,
          "repeatCount": 1,
          "baseScore": 500,
          "midiVelocityOrb": false,
          "midiVelocityAddScore": 0,
          "normalizedMidiVelocity": 0.0,
          "baseScoreForMidiVelocityOrb": 0,
          "bonusOrbVelocity": 50,
          "bonusOrbRotation": 50,
          "bonusHead": 200,
          "bonusFeet": 100,
          "headOfftimeMultiplier": 0.5,
          "bonusOrbDistance": 400,
          "bonusSlider": 400,
          "bonusSpinner": 1200,
          "caught": true,
          "timingOffset": 0.0,
          "timingOffsetWithHeadTolerance": 0.0,
          "caughtWith": 1,
          "caughtWithDifferent": 1,
          "caughtWithDifferentBonus": 50,
          "catchTime": 9.899024963378907,
          "catchPosition": {
            "x": -0.20230330526828767,
            "y": 1.8836393356323243,
            "z": -1.3598828315734864
          },
          "comboCount": 1,
          "currentMultiplier": 1,
          "rawScoreWithBonus": 555,
          "timedScoreWithBonus": 555,
          "actualBonusHead": 0,
          "actualBonusFeet": 0,
          "velocityBonusFactor": 0.0,
          "actualVelocityBonusScore": 0,
          "rotationBonusFactor": 0.0,
          "actualRotationBonusScore": 0,
          "distance": 0.0,
          "distanceMultiplier": 0.0,
          "distanceByTimeMultiplier": 0.0,
          "actualBonusOrbDistance": 0,
          "actualBonusSlider": 0,
          "actualBonusSpinner": 0,
          "accumulatedTime": 0.30316162109375,
          "scoreWithMultiplier": 555.0,
          "balanceMultiplier": 1.0,
          "finalScore": 555,
          "totalScore": 555
        },
// example for an orb:
        {
          "eventIndex": 2,
          "touchType": 0,
          "pickupType": 0,
          "platformPositionAtInstantiation": {
            "x": 0.0,
            "y": 0.25,
            "z": -2.0
          },
          "platformRotationAtInstantiation": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0,
            "w": 1.0
          },
          "instantiateTime": 9.291020393371582,
          "instantiatePosition": {
            "x": -0.9453125,
            "y": 2.515625,
            "z": 13.0
          },
          "perfectCatchTime": 10.488000869750977,
          "perfectCatchPosition": {
            "x": -0.354146271944046,
            "y": 1.7074555158615113,
            "z": -1.3721325397491456
          },
          "noteDuration": 0.10000000149011612,
          "flyDuration": 1.1969804763793946,
          "showTimingDuration": 0.4510999917984009,
          "repeatCount": 0,
          "baseScore": 300,
          "midiVelocityOrb": false,
          "midiVelocityAddScore": 0,
          "normalizedMidiVelocity": 0.0,
          "baseScoreForMidiVelocityOrb": 0,
          "bonusOrbVelocity": 50,
          "bonusOrbRotation": 50,
          "bonusHead": 200,
          "bonusFeet": 100,
          "headOfftimeMultiplier": 0.5,
          "bonusOrbDistance": 400,
          "bonusSlider": 400,
          "bonusSpinner": 1200,
          "caught": true,
          "timingOffset": 0.01945972442626953,
          "timingOffsetWithHeadTolerance": 0.0,
          "caughtWith": 1,
          "caughtWithDifferent": 0,
          "caughtWithDifferentBonus": 0,
          "catchTime": 10.507460594177246,
          "catchPosition": {
            "x": -0.34979888796806338,
            "y": 1.701512336730957,
            "z": -1.4778237342834473
          },
          "comboCount": 3,
          "currentMultiplier": 1,
          "rawScoreWithBonus": 650,
          "timedScoreWithBonus": 611,
          "actualBonusHead": 0,
          "actualBonusFeet": 0,
          "velocityBonusFactor": 0.35585516691207888,
          "actualVelocityBonusScore": 15,
          "rotationBonusFactor": 0.14098197221755982,
          "actualRotationBonusScore": 6,
          "distance": 1.2164618968963624,
          "distanceMultiplier": 0.8171582221984863,
          "distanceByTimeMultiplier": 0.0,
          "actualBonusOrbDistance": 326,
          "actualBonusSlider": 0,
          "actualBonusSpinner": 0,
          "accumulatedTime": 0.0,
          "scoreWithMultiplier": 611.0,
          "balanceMultiplier": 1.0,
          "finalScore": 611,
          "totalScore": 1677
        },
