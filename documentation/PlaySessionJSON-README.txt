This files is a documentation for play session meta data that you find in
GameData/PlaySessions.json (you can configure the full path on Configuration.json)

version
Version of this file. Currently 1; will be incremented every time we change the
format.

gameVersion
Version of Holodance that this file was created with.

artist
Artist of the song that was played.

title
Name of the song that was played.

beatmapVersion
Human readable beatmap version. This was assigned by the author of the beatmap.

difficulty
Currently, this is "Overall Difficulty". We'll have our own better way of 
calculating beatmap difficulties soon.

playedDate
Date when this session was completed. This is basically DateTime.Now.Ticks:
https://msdn.microsoft.com/en-us/library/system.datetime.ticks(v=vs.110).aspx
This value is also used as filename for the gameplay recording.
You can look up the recording for this session by looking in the folder:
PlaySessions/{gameType}/{levelId}/{trackId}/{playedDate}.json.gz

playerSelf
Information about the player that has recorded this session.

allPlayers
List of all players that participated in this session. This will be more 
interesting once we have multiplayer ;-)


gameType
What kind of game mode was used to create this beatmap. Several other values
depend on this.

    public enum GameType {
        Holodance = 0, // Story Mode
        HolodanceCustom = 1, // Player Created Beatmaps in Holodance
        osu = 2, // osu! beatmaps
        StepMania = 3 // StepMania beatmaps
    }

levelId
For gameType = 0, this is the actual level in Story Mode. For HolodanceCustom = 1,
this is the song-ID. For osu = 2, this is currently the beatmapSetId. StepMania
is not supported, yet.

trackId
For gameType = 0, this is the track (0-3) in Story Mode. For HolodanceCustom = 1,
this is the beatmap-ID. For osu = 2, this is currently the beatmapId. StepMania
is not supported, yet.

beatmapHash
MD5-Hash of the beatmap file.

songLengthSeconds
Length of the song in seconds.

cancelled
Was this session cancelled (true|false)?

cancelledTime
If the session was cancelled, at what time?

environment
Which environment was used for playing this session?

environmentVersion
No longer relevant ... we used this for levels 1 and 3 for performance 
optimization but now this is handled differently.

bufferAheadQuarters
How many quarters before the orb has to be caught or the Slider starts is the
"flying note" created? How long this is as time depends on the BPM of the song.
At 120BPM, 4 would mean 2 seconds. At 180 BPM, 4 would mean 1.5 seconds.

orbSourceDistance
How far from the player was the "orb source" (where the orbs are coming from)
during this session. bufferAheadQuarters together with orbSourceDistance
determines how fast the orbs are flying at you. We have some environments where
you can change the orb source location (Zero Distraction, Virtual Clubbing),
and we also have environments where the orbs come from different locations,
so the distance may be different at different times of the session.

playerCount
How many players took part in this session?

currentMultiplier
Combo multiplayer when the session was ended.

hitNotesWithNoMiss
Combo length when the session was ended.

maxHitNotesWithNoMiss
Longest combo in this session.

sessionPauseCount
How often was the game paused during this session.

currentScore
The current (and probably final ;-) ) score in this session.

previousLeaderboardRank
Previous rank in the leaderboard, if any.

newLeaderboardRank
Rank in the leaderboard after this session.

leaderBoardEntryCount
How many entries does this leaderboard have?

hitNotes
Number of "flying notes" that were caught. This is the sum of orbs, sliders
and spinners.

hitOrbs
Number of orbs that were caught.

hitSliders
Number of sliders that were caught.

hitSpinners
Number of spinners that were caught.

missedNotes
Number of "flying notes" that were missed. This is the sum of orbs, sliders
and spinners.

missedOrbs
Number of orbs that were missed.

missedSliders
Number of sliders that were missed.

missedSpinners
Number of spinners that were missed.

hitNotesWithHead
hitNotesWithLeftHand
hitNotesWithRightHand
hitNotesWithLeftFoot
hitNotesWithRightFoot
hitNotesWithUnknownHand
How were the flying notes caught?

hitNotesForAverage
Number of notes that were caught used to calculate the average values. Should 
be the same value as hitNotes.

cumulativeOffTime
The accumulated time in seconds that each note was missed by. This is basically
your accuracy, without normalization (so the more orbs a song has, the higher 
the value). The lower this value, the higher your accuracy.

avgOffTime
This is the normalized inverted accuracy. How much time do you have on average
between when the ideal hit time of an orb is compared to when you actually
caught it.

maxOffTime
The longest time it took between when an orb should have been caught and when
you actually caught it.

minOffTime
The shortest time it took between when an orb should have been caught and when
you actually caught it.

totalDistanceHead
totalDistanceLeft
totalDistanceRight
totalDistanceLeftFoot
totalDistanceRightFoot
totalDistanceUnknown
How much distance in meters have you covered with each tracked device.


Example:

{
    "version": 0,
    "gameVersion": "",
    "artist": "Muzzy",
    "title": "Draining Atlantic",
    "beatmapVersion": "Don't forget me.",
    "difficulty": 8.0,
    "playedDate": 636251312833178652,
    "playerSelf": {
        "version": 0,
        "playerPlatforms": 1,
        "steamID": 76561198091442463,
        "nickname": "jashan",
        "playerHeight": 1.8186225891113282,
        "playerChestHeight": 1.399999976158142
    },
    "allPlayers": [
        {
            "version": 0,
            "playerPlatforms": 1,
            "steamID": 76561198091442463,
            "nickname": "jashan",
            "playerHeight": 1.8186225891113282,
            "playerChestHeight": 1.399999976158142
        }
    ],
    "gameType": 2,
    "levelId": 479671,
    "trackId": 1024215,
    "beatmapHash": "",
    "songLengthSeconds": 376.52801513671877,
    "cancelled": false,
    "cancelledTime": 0.0,
    "environment": "Level-00-Neutral",
    "environmentVersion": "Level-00-Neutral",
    "bufferAheadQuarters": 8,
    "orbSourceDistance": 15.0,
    "playerCount": 1,
    "currentMultiplier": 2,
    "hitNotesWithNoMiss": 8,
    "maxHitNotesWithNoMiss": 326,
    "sessionPauseCount": 0,
    "currentScore": 818248,
    "previousLeaderboardRank": 0,
    "newLeaderboardRank": 0,
    "leaderBoardEntryCount": 0,
    "hitNotes": 839,
    "hitOrbs": 362,
    "hitSliders": 474,
    "hitSpinners": 3,
    "missedNotes": 5,
    "missedOrbs": 3,
    "missedSliders": 2,
    "missedSpinners": 0,
    "hitNotesWithHead": 0,
    "hitNotesWithLeftHand": 433,
    "hitNotesWithRightHand": 406,
    "hitNotesWithLeftFoot": 0,
    "hitNotesWithRightFoot": 0,
    "hitNotesWithUnknownHand": 0,
    "hitNotesForAverage": 839,
    "cumulativeOffTime": 7.539276123046875,
    "avgOffTime": 0.008986026048660279,
    "maxOffTime": 0.0789794921875,
    "minOffTime": 0.0,
    "totalDistanceHead": 0.0,
    "totalDistanceLeft": 0.0,
    "totalDistanceRight": 0.0,
    "totalDistanceLeftFoot": 0.0,
    "totalDistanceRightFoot": 0.0,
    "totalDistanceUnknown": 0.0,
    "replayData": []
},
