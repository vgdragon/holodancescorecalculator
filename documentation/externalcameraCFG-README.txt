We have added some extra settings to these cfg-files:

fps=30
renderPostFXforVRNot=True
renderPostFXforMR=True

"fps" can be used to limit rendering to the MR screen with the given FPS.
Use this only when you record at 30 FPS (with 60 FPS, it's better to not
add a limit here because you already play a game with the 90 FPS HMD sync).

"renderPostFXforVRNot" removes post-fx in VR so there is more GPU-power for
rendering the Mixed Reality output. Default is "False" because it makes the
game look much worse in VR. But if you do streaming and hit performance limits,
this may be your friend.

"renderPostFXforMR" adds post-fx to the Mixed Reality output. You should activate
this but it may give you trouble with performance, so the default is false.

