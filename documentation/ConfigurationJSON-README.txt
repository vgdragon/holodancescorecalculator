This files is a documentation for Configuration.json:

General note: We currently auto-convert relative paths to full paths. So you 
can enter a relative path and it should work but next time you open the file, 
the path will be absolute. You can use both "/" and "\\" as path separator. 
"\\" is equivalent to "\" (but these JSON-files require escaping the "\",
so that's why it's "\\")

version
Incremental version number; used for auto-updating these files when we make 
changes. Don't mess with this. Current version: 5

pathPartyModeHighscores
Full (or relative) path to a text-file that stores local highscores. Holodance 
automatically appends strings for different party mode presets and daily
leaderboards.

pathLocalMP3s
Not used, yet, but this is the base path for your local MP3s. We'll probably 
make this a list because not everyone has all the MP3s in only one folder, 
even if most people will.

useOsuDefaultFolder
This is osu! folder based on an osu! installation. Currently, this is always
true but you can set it to false to disable looking for an osu! installation.
Eventually, Holodance will automatically set this to false if osu! is not
found.

useOsuDbForDefaultFolder
This is a very important one: If you have osu! installed, you'll usually want
this to be true. If you set it to false, instead of using osu!.db, Holodance
will scan all beatmap-folders and beatmaps to gather the information. For just
a few songs, that's fine - with a large library (100 GB, 10K+ songs, 45K+ 
beatmaps), this might take hours. Also, some information like the length of
songs in seconds is only available when using osu!.db until the beatmap was 
played for the first time (because otherwise, the scanning would take even
much longer).

pathOsuSongs
This is the base path where the beatmaps are stored for the "default" 
installation. This will be where osu! looks for the songs; it is overwritten
with the data from the registry. If this gives you a wrong value, you can
usually fix this by simply starting osu! once (happens e.g. when osu! was
moved; osu! fixes the relevant registry entries on startup).

useOsuExtraFolder
With the "osu! extra folder", Holodance can have its own folder for osu! 
beatmaps. You will always want to use this when you don't have osu! installed,
and you could even use it to keep the beatmaps in osu! separate from those
in Holodance. You can also use both.

autoDownloadOsuBeatmaps
When this is "true", Holodance will automatically try to download beatmaps
from favorites or last played when they are not available.

pathOsuSongsExtra
This is where the osu! beatmaps used only by Holodance are stored. Usually:
\\GameData\\Songs\\osu\\, usually where your Holodance installation is on
Steam. But you can use Configuration.json to define another base path for
"GameData".

pathStepmaniaSongs
Not supported, yet.

pathPlaySessions
Path where PlaySessions.json is stored. See PlaySessionsJSON-README.txt for
documentation.

pathPlaySessionsText
This is a human readable text-file with statistical data about your play 
sessions. You can use this to analyze how you did playing each session.

pathPlaySessionsBaseFolder
This is the folder where all the replay- and scoring-data for each session is
stored.

pathOsuJsonFileSystemCache
This is the path to our version of osu!.db. Basically a file with the meta-data
for all beatmaps. It is generated either by reading osu!.db (very fast), or by 
scanning the file system and reading all beatmaps (very slow). This is the 
meta-data for the beatmaps available in the osu!-installation (if you have osu!)
installed on your machine.

pathOsuJsonFileSystemCacheExtra
Same as pathOsuJsonFileSystemCache but for the "extra" folder, which is the 
path where you can keep beatmaps only available to Holodance.

pathOsuJsonFavorites
Path to your favorites.

pathOsuJsonPlaySessions
Path to your osu!-only play sessions.

updateCurrentScoreToFile
If you set this to true, Holodance will frequently write the current song and
score information to the file defined in pathCurrentScoreFile. This is useful
for streamers that want to show this information in their own UI overlay for 
the stream.

forceWriteScoreToFile
If false, this will only write to the file when there were changes. If true,
it will always write to the file after updateCurrentScoreFrequencySeconds.

updateCurrentScoreFrequencySeconds
Number of seconds to wait between writing to the score file.

pathCurrentScoreFile
Path to the score file.



Example:

{
    "version": 5,
    "pathPartyModeHighscores": "D:\\Steam\\steamapps\\common\\Holodance\\Holodance_Data\\LocalHighScores.txt",
    "pathLocalMP3s": "",
    "useOsuDefaultFolder": true,
    "useOsuDbForDefaultFolder": true,
    "pathOsuSongs": "C:\\temp\\Songs-Compressed",
    "useOsuExtraFolder": true,
    "autoDownloadOsuBeatmaps": true,
    "pathOsuSongsExtra": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\Songs\\osu\\",
    "pathStepmaniaSongs": "",
    "pathPlaySessions": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\PlaySessions.json",
    "pathPlaySessionsText": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\PlaySessions.txt",
    "pathPlaySessionsBaseFolder": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\PlaySessions\\",
    "pathOsuJsonFileSystemCache": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\FileSystemCache_Osu.json",
    "pathOsuJsonFileSystemCacheExtra": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\FileSystemCache_Osu_Extra.json",
    "pathOsuJsonFavorites": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\FileSystemCache_Osu_Favorites.json",
    "pathOsuJsonPlaySessions": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\FileSystemCache_Osu_LastPlayed.json",
    "updateCurrentScoreToFile": false,
    "forceWriteScoreToFile": true,
    "updateCurrentScoreFrequencySeconds": 5,
    "pathCurrentScoreFile": "D:\\Steam\\steamapps\\common\\Holodance\\GameData\\CurrentScore.json"
}