package com.vgdragon.holodanceScoreCalculator.Ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChooseUi {
    private JPanel jpanel;
    private JButton scoreCalculatorButton;
    private JButton statsButton;

    private static JFrame frame;


    public static void chooseUi(){
        frame = new JFrame("Choose Program");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new ChooseUi().jpanel);
        frame.setVisible(true);
        frame.setSize(new Dimension(100,100));
    }


    public ChooseUi() {



        scoreCalculatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UI.ui();
                frame.setVisible(false);
            }
        });

        statsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BeBetterUi.ui();
                frame.setVisible(false);
            }
        });


    }








}
