package com.vgdragon.holodanceScoreCalculator.Ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vgdragon.holodanceScoreCalculator.Data;
import com.vgdragon.javadance.Configuration.HolodanceJson;
import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.PlaySession;
import com.vgdragon.javadance.Configuration.JsonClasses.PlaySessionsJson;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayJson;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class UISetBeatMap {

    private JPanel panel;

    private JButton loadReplayButton;
    private JTextField folderTextField;
    private JButton folderButton;
    private JButton setReplayButton;
    private JList replayJList;
    private JLabel errorTestField;


    static UI mainUi;
    static BeBetterUi beBetterUi;
    static boolean isMainUi;

    private static JFrame frame;

    private static Data data = new Data();
    PlaySessionsJson playSessionsJson;

    HolodanceJson holodanceJson = new HolodanceJson();

    public static void ui(){
        frame = new JFrame("Choice Beatmap");
        frame.setContentPane(new UISetBeatMap().panel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        frame.setSize(new Dimension(500, 500));

    }

    public static void ui(UI ui, Data dataClass){
        data = dataClass;
        mainUi = ui;
        isMainUi = true;
        ui();
    }
    public static void ui(BeBetterUi ui, Data dataClass){
        data = dataClass;
        beBetterUi = ui;
        isMainUi = false;
        ui();
    }



    public UISetBeatMap(){
        setOpenUiSettings();

        folderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser jFileChooser = new JFileChooser();
                jFileChooser.setCurrentDirectory(new File(""));
                jFileChooser.setDialogTitle("Select Holodance Folder.");
                jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jFileChooser.setAcceptAllFileFilterUsed(false);

                if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    folderTextField.setText(String.valueOf(jFileChooser.getSelectedFile()));


                    try {

                        data.setHolodanceFolder(folderTextField.getText());
                        new ObjectMapper().writeValue(new File(data.getDataFolder() + data.getDataFileName()), data);


                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                }


            }
        });


        loadReplayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(!folderTextField.getText().equalsIgnoreCase("")){

                    playSessionsJson = new PlaySessionsJson();
                    try {
                        playSessionsJson = holodanceJson.getPlaySessionsJson(folderTextField.getText());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    List<PlaySession> playSessions = playSessionsJson.getPlaySessions();
                    DefaultListModel listModel = new DefaultListModel();

                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");


                    for (int i = 0; i < playSessions.size(); i++) {
                        PlaySession playSession = playSessions.get(i);
                        if(playSession.getVersion() > 0
                                || playSession.getPlayedDate() > 636341810101645851L
                                ){
                            listModel.addElement("<html>Song Name: " + playSession.getArtist()
                                    + " - " + playSession.getTitle()
                                    + "<br> Beatmap Version: " +playSession.getBeatmapVersion()
                                    + "<br> Played from: " + playSession.getPlayerSelf().getNickname()
                                    + "<br> Played at: " + dateFormat.format(new Date((playSession.getPlayedDate() - 621355968000000000L) / 10000))
                                    + "<br>-------------------</html>");
                        } else {
                            playSessions.remove(i);
                            i--;
                        }

                    }
                    replayJList.setModel(listModel);
                    if(playSessionsJson.getPlaySessions().size() > 0){
                        replayJList.setSelectedIndex(0);
                        setReplayButton.setEnabled(true);
                    }

                    errorTestField.setText("Select a Replay.");




                }


            }
        });


        setReplayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    ReplayJson replayJson = holodanceJson.getReplayJson(data.getHolodanceFolder(), (playSessionsJson.getPlaySessions().get(replayJList.getSelectedIndex())));
                    if(isMainUi) {
                        mainUi.setReplayJson(replayJson);
                    } else {
                        beBetterUi.setReplayJson(replayJson);
                    }

                    frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));

                } catch (IOException e1) {
                    errorTestField.setText("A problem to load the Replay happen.");
                    e1.printStackTrace();
                }

            }
        });

    }

    private void setOpenUiSettings(){
        loadReplayButton.setEnabled(true);
        setReplayButton.setEnabled(false);
        folderTextField.setText(data.getHolodanceFolder());




    }





}
