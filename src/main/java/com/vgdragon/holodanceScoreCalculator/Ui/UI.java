package com.vgdragon.holodanceScoreCalculator.Ui;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.vgdragon.holodanceScoreCalculator.Data;
import com.vgdragon.holodanceScoreCalculator.scorePackage.CalculateScore;
import com.vgdragon.holodanceScoreCalculator.scorePackage.Settings.MathSettingsListClass;
import com.vgdragon.holodanceScoreCalculator.scorePackage.UiScoreVariablesInput;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses.RecordedEvent;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses.ReplayData;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayJson;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UI {


    private static JFrame frame;
    private javax.swing.JPanel panel;

    private JButton loadReplayFileButton;

    private JList replayList;
    private JButton loadReplayButton;

    private JButton settingsSwithButton;
    private JButton settingsButton;

    private JButton calculateScoreButton;
    private JTextField endScoreText;
    private JLabel infoText;

    private JPanel settingsPanel;

    // Basic Settings

    private JPanel basicplaceholder;

    private JTextField basicBaseScoreTextField;
    private JTextField basicBonusOrbRotationTextField;
    private JTextField basicBonusFeetTextField;
    private JTextField basicBonusSliderTextField;
    private JTextField basicBonusOrbVelocityTextField;
    private JTextField basicBonusOrbDistanceTextField;
    private JTextField basicBonusSpinnerTextField;
    private JTextField basicBonusHeadTextField;
    private JTextField basicCaughtWithDifferent1TextField;
    private JTextField basicCaughtWithDifferent2TextField;
    private JTextField basicCaughtWithDifferent3TextField;
    private JTextField basicCaughtWithDifferent4TextField;


    private JList basicMathSettingsCalculationJList;
    private JList basicMathSettingsVariableJList;
    private JButton basicAddMathSettingButton;
    private JButton basicDeleteMathSettingButton;
    private JList basicMathSettingJList;


    // Advanced Settings


    private JButton orbAnySettingsButton;
    private JButton orbHandSettingsButton;
    private JButton orbHeadSettingsButton;
    private JButton orbFootSettingsButton;

    private JLabel settingsPickupTypeSettingText;

    //

    private JButton defaultButton;
    private JButton basicDeleteAllMathSettingButton;
    private JPanel adcancedSettingsButtonPanel;
    private JLabel settingsErrorText;
    private JPanel errorTextPlaceholder;

    //

    private String txtFileNameCustom = "HolodanceScoreCalculatorData.txt";
    private String txtFileNameDefault = "HolodanceScoreCalculatorDataDefault.txt";
    private String txtFileName;



    //basic = -1;
    //Any = 0
    //Hand = 1
    //Head = 2
    //Foot = 3
    private int pickupTypeSetting = -1;

    private ReplayJson replayJson;
    private ReplayData replayData;
    private List<RecordedEvent> recordedEventsList;

    private UiScoreVariablesInput uiScoreVariablesInput = new UiScoreVariablesInput();

    private List<String> calculatingList = new ArrayList<>();

    private MathSettingsListClass mathSettingsListClass = new MathSettingsListClass();

    // false = basic
    // true = advanced
    private boolean showedAdvancedSettings = false;
    private boolean defaultSettings = false;

    Data data = new Data();

    public static void ui() {
        frame = new JFrame("Holodance Score Calculator");
        frame.setContentPane(new UI().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setVisible(true);
        frame.setMinimumSize(new Dimension(520, 620));
        frame.setMaximumSize(new Dimension(520, 620));
    }

    public UI() {

        startSettings();

        loadReplayFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UISetBeatMap.ui(UI.this, data);

            }
        });
        loadReplayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadReplayButton.setEnabled(false);
                replayData = replayJson.getReplayData().get(replayList.getSelectedIndex());


                recordedEventsList = new ArrayList<>();

                Thread t = new Thread() {
                    public void run() {

                        recordedEventsList = replayData.getRecordedEvents();

                        loadReplayButton.setEnabled(true);
                        calculateScoreButton.setEnabled(true);
                        infoText.setText("Ready.");
                    }

                };

                t.start();


            }
        });

        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (settingsPanel.isVisible()) {
                    settingsPanel.setVisible(false);
                    panel.setMinimumSize(new Dimension(500, 600));
                    panel.setMaximumSize(new Dimension(500, 600));
                    frame.setMinimumSize(new Dimension(520, 620));
                    frame.setMaximumSize(new Dimension(520, 620));
                    frame.setSize(new Dimension(520, 620));
                } else {
                    settingsPanel.setVisible(true);
                    panel.setMinimumSize(new Dimension(1240, 600));
                    panel.setMaximumSize(new Dimension(1240, 600));
                    frame.setMinimumSize(new Dimension(1240, 620));
                    frame.setMaximumSize(new Dimension(1240, 620));
                    frame.setSize(new Dimension(1240, 620));
                }
            }
        });
        settingsSwithButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (showedAdvancedSettings) {
                    if (putAwayAdvancedUiScoreVariablesInput()) {
                        pickupTypeSetting = -1;
                        basicplaceholder.setVisible(true);
                        settingsPickupTypeSettingText.setVisible(false);
                        adcancedSettingsButtonPanel.setVisible(false);
                        showedAdvancedSettings = false;
                        settingsSwithButton.setText("Advanced Settings");

                        putInUiAdvancedUiScoreVariablesInput();
                    }

                } else {
                    if (putAwayAdvancedUiScoreVariablesInput()) {
                        pickupTypeSetting = 0;
                        basicplaceholder.setVisible(false);
                        settingsPickupTypeSettingText.setVisible(true);
                        adcancedSettingsButtonPanel.setVisible(true);
                        showedAdvancedSettings = true;
                        settingsSwithButton.setText("Basic Settings");

                        putInUiAdvancedUiScoreVariablesInput();
                    }

                }
            }
        });

        calculateScoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                calculateScoreButton.setEnabled(false);

                Thread t = new Thread() {
                    public void run() {

                        if (showedAdvancedSettings) {
                            if (putAwayAdvancedUiScoreVariablesInput()) {
                                CalculateScore calculateScore = new CalculateScore();
                                endScoreText.setText(formatScore(calculateScore.calculateScoreAdvanced(recordedEventsList, uiScoreVariablesInput, mathSettingsListClass, infoText)));

                            }
                        } else {
                            if (putAwayAdvancedUiScoreVariablesInput()) {
                                CalculateScore calculateScore = new CalculateScore();
                                endScoreText.setText(formatScore(calculateScore.calculateScoreBasic(recordedEventsList, uiScoreVariablesInput, mathSettingsListClass, infoText)));


                            }

                        }

                        calculateScoreButton.setEnabled(true);

                    }
                };

                t.start();

            }
        });

        // basic Options

//        basicAddMathSettingButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                mathSettingsListClass.basicMathSettingsList.add(basicMathSettingsCalculationJList.getSelectedValue().toString() + ";" + basicMathSettingsVariableJList.getSelectedValue().toString());
//                updateBasicMathSettingJList();
//            }
//        });
//        basicDeleteMathSettingButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                mathSettingsListClass.basicMathSettingsList.remove(basicMathSettingJList.getSelectedIndex());
//                updateBasicMathSettingJList();
//
//            }
//        });
//        basicDeleteAllMathSettingButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                mathSettingsListClass.basicMathSettingsList = new ArrayList<>();
//                updateBasicMathSettingJList();
//
//            }
//        });
//
        // advanced Options

        orbAnySettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (putAwayAdvancedUiScoreVariablesInput()) {

                    pickupTypeSetting = 0;
                    putInUiAdvancedUiScoreVariablesInput();
                }

            }
        });
        orbHandSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (putAwayAdvancedUiScoreVariablesInput()) {

                    pickupTypeSetting = 1;
                    putInUiAdvancedUiScoreVariablesInput();
                }
            }
        });
        orbHeadSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (putAwayAdvancedUiScoreVariablesInput()) {

                    pickupTypeSetting = 2;
                    putInUiAdvancedUiScoreVariablesInput();
                }
            }
        });
        orbFootSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (putAwayAdvancedUiScoreVariablesInput()) {

                    pickupTypeSetting = 3;
                    putInUiAdvancedUiScoreVariablesInput();
                }
            }
        });

        basicAddMathSettingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculatingList.add(basicMathSettingsCalculationJList.getSelectedValue().toString() + ";" + basicMathSettingsVariableJList.getSelectedValue().toString());
                updateBasicMathSettingJList();

            }
        });
        basicDeleteMathSettingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculatingList.remove(basicMathSettingJList.getSelectedIndex());
                updateBasicMathSettingJList();

            }
        });
        basicDeleteAllMathSettingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculatingList = new ArrayList<>();
                updateBasicMathSettingJList();
            }
        });

        defaultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(defaultSettings){
                    defaultButton.setText("Default Settings");
                    defaultSettings = false;
                    setCustomSettings();

                } else {
                    if(setDefaultSettings()) {
                        defaultButton.setText("Custom Settings");
                        defaultSettings = true;
                    }

                }

            }
        });



    }


    private void startSettings() {
        try {
            data = new ObjectMapper().readValue(new File(data.getDataFolder() + data.getDataFileName()), Data.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        txtFileName = txtFileNameCustom;
        
        panel.setMinimumSize(new Dimension(520, 600));
        panel.setMaximumSize(new Dimension(520, 600));
        settingsPanel.setMaximumSize(new Dimension(700, 600));
        settingsPanel.setMinimumSize(new Dimension(700, 600));

        defaultButton.setText("Default");
        settingsSwithButton.setText("Advanced Settings");
        infoText.setText("Set Replay File.");
        

        settingsPanel.setVisible(false);
        defaultSettings = false;
        pickupTypeSetting = -1;
        showedAdvancedSettings = false;
        basicplaceholder.setVisible(true);
        adcancedSettingsButtonPanel.setVisible(false);
        settingsPickupTypeSettingText.setVisible(false);
        
        settingsErrorText.setText("");
        errorTextPlaceholder.setVisible(true);
        

        DefaultListModel tempMathSettingsCalculationList = new DefaultListModel();
        tempMathSettingsCalculationList.addElement("+");
        tempMathSettingsCalculationList.addElement("-");
        tempMathSettingsCalculationList.addElement("*");
        tempMathSettingsCalculationList.addElement("/");
        basicMathSettingsCalculationJList.setModel(tempMathSettingsCalculationList);
        basicMathSettingsCalculationJList.setSelectedIndex(0);

        DefaultListModel tempMathSettingsVariableList = new DefaultListModel();

        tempMathSettingsVariableList.addElement("(");
        tempMathSettingsVariableList.addElement(")");

        tempMathSettingsVariableList.addElement("baseScore");
        tempMathSettingsVariableList.addElement("bonusHead");
        tempMathSettingsVariableList.addElement("bonusFeet");
        tempMathSettingsVariableList.addElement("bonusSlider");
        tempMathSettingsVariableList.addElement("bonusSpinner");
        tempMathSettingsVariableList.addElement("bonusOrbDistance");
        tempMathSettingsVariableList.addElement("bonusOrbVelocity");
        tempMathSettingsVariableList.addElement("bonusOrbRotation");
        tempMathSettingsVariableList.addElement("caughtWithDifferent");

        tempMathSettingsVariableList.addElement("noteDuration");
        tempMathSettingsVariableList.addElement("accumulatedTime");
        tempMathSettingsVariableList.addElement("rotationBonusFactor");
        tempMathSettingsVariableList.addElement("velocityBonusFactor");
        tempMathSettingsVariableList.addElement("timingOffset");
        tempMathSettingsVariableList.addElement("distance");
        tempMathSettingsVariableList.addElement("distanceMultiplier");
        tempMathSettingsVariableList.addElement("caughtWithDifferentBonus");
        tempMathSettingsVariableList.addElement("currentMultiplier");



        basicMathSettingsVariableJList.setModel(tempMathSettingsVariableList);
        basicMathSettingsVariableJList.setSelectedIndex(0);

        loadFromTxt();
        putInUiAdvancedUiScoreVariablesInput();

        //calculatingList = mathSettingsListClass.basicMathSettingsList;

    } //// done

    private boolean isUiUiScoreVariablesAllNumbers() {

            try {
                int tempInt = 0;

                ////////// done
                tempInt = Integer.parseInt(basicBaseScoreTextField.getText());
                tempInt = Integer.parseInt(basicBonusOrbVelocityTextField.getText());
                tempInt = Integer.parseInt(basicBonusOrbRotationTextField.getText());
                tempInt = Integer.parseInt(basicBonusHeadTextField.getText());
                tempInt = Integer.parseInt(basicBonusFeetTextField.getText());
                tempInt = Integer.parseInt(basicBonusOrbDistanceTextField.getText());
                tempInt = Integer.parseInt(basicBonusSliderTextField.getText());
                tempInt = Integer.parseInt(basicBonusSpinnerTextField.getText());
                tempInt = Integer.parseInt(basicCaughtWithDifferent1TextField.getText());
                tempInt = Integer.parseInt(basicCaughtWithDifferent2TextField.getText());
                tempInt = Integer.parseInt(basicCaughtWithDifferent3TextField.getText());
                tempInt = Integer.parseInt(basicCaughtWithDifferent4TextField.getText());


            } catch (Exception e) {
                settingsErrorText.setText("One or more Settings are not numbers.");
                errorTextPlaceholder.setVisible(false);
                return false;
            }


        settingsErrorText.setText("");
        errorTextPlaceholder.setVisible(true);
        return true;

    } // done

    private boolean putAwayAdvancedUiScoreVariablesInput() {
        if (isUiUiScoreVariablesAllNumbers()) {

            if (pickupTypeSetting == -1) {
                uiScoreVariablesInput.basicBaseScore = Integer.parseInt(basicBaseScoreTextField.getText());
                uiScoreVariablesInput.basicBonusOrbVelocity = Integer.parseInt(basicBonusHeadTextField.getText());
                uiScoreVariablesInput.basicBonusOrbRotation = Integer.parseInt(basicBonusFeetTextField.getText());
                uiScoreVariablesInput.basicBonusHead = Integer.parseInt(basicBonusSliderTextField.getText());
                uiScoreVariablesInput.basicBonusFeet = Integer.parseInt(basicBonusSpinnerTextField.getText());
                uiScoreVariablesInput.basicBonusOrbDistance = Integer.parseInt(basicBonusOrbDistanceTextField.getText());
                uiScoreVariablesInput.basicBonusSlider = Integer.parseInt(basicBonusOrbVelocityTextField.getText());
                uiScoreVariablesInput.basicBonusSpinner = Integer.parseInt(basicBonusOrbRotationTextField.getText());
                uiScoreVariablesInput.basicCaughtWithDifferent1 = Integer.parseInt(basicCaughtWithDifferent1TextField.getText());
                uiScoreVariablesInput.basicCaughtWithDifferent2 = Integer.parseInt(basicCaughtWithDifferent2TextField.getText());
                uiScoreVariablesInput.basicCaughtWithDifferent3 = Integer.parseInt(basicCaughtWithDifferent3TextField.getText());
                uiScoreVariablesInput.basicCaughtWithDifferent4 = Integer.parseInt(basicCaughtWithDifferent4TextField.getText());
            } else if (pickupTypeSetting == 0) {
                uiScoreVariablesInput.advancedAnyBaseScore = Integer.parseInt(basicBaseScoreTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusOrbVelocity = Integer.parseInt(basicBonusHeadTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusOrbRotation = Integer.parseInt(basicBonusFeetTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusHead = Integer.parseInt(basicBonusSliderTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusFeet = Integer.parseInt(basicBonusSpinnerTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusOrbDistance = Integer.parseInt(basicBonusOrbDistanceTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusSlider = Integer.parseInt(basicBonusOrbVelocityTextField.getText());
                uiScoreVariablesInput.advancedAnyBonusSpinner = Integer.parseInt(basicBonusOrbRotationTextField.getText());
                uiScoreVariablesInput.advancedAnyCaughtWithDifferent1 = Integer.parseInt(basicCaughtWithDifferent1TextField.getText());
                uiScoreVariablesInput.advancedAnyCaughtWithDifferent2 = Integer.parseInt(basicCaughtWithDifferent2TextField.getText());
                uiScoreVariablesInput.advancedAnyCaughtWithDifferent3 = Integer.parseInt(basicCaughtWithDifferent3TextField.getText());
                uiScoreVariablesInput.advancedAnyCaughtWithDifferent4 = Integer.parseInt(basicCaughtWithDifferent4TextField.getText());
            } else if (pickupTypeSetting == 1) {
                uiScoreVariablesInput.advancedHandBaseScore = Integer.parseInt(basicBaseScoreTextField.getText());
                uiScoreVariablesInput.advancedHandBonusOrbVelocity = Integer.parseInt(basicBonusHeadTextField.getText());
                uiScoreVariablesInput.advancedHandBonusOrbRotation = Integer.parseInt(basicBonusFeetTextField.getText());
                uiScoreVariablesInput.advancedHandBonusHead = Integer.parseInt(basicBonusSliderTextField.getText());
                uiScoreVariablesInput.advancedHandBonusFeet = Integer.parseInt(basicBonusSpinnerTextField.getText());
                uiScoreVariablesInput.advancedHandBonusOrbDistance = Integer.parseInt(basicBonusOrbDistanceTextField.getText());
                uiScoreVariablesInput.advancedHandBonusSlider = Integer.parseInt(basicBonusOrbVelocityTextField.getText());
                uiScoreVariablesInput.advancedHandBonusSpinner = Integer.parseInt(basicBonusOrbRotationTextField.getText());
                uiScoreVariablesInput.advancedHandCaughtWithDifferent1 = Integer.parseInt(basicCaughtWithDifferent1TextField.getText());
                uiScoreVariablesInput.advancedHandCaughtWithDifferent2 = Integer.parseInt(basicCaughtWithDifferent2TextField.getText());
                uiScoreVariablesInput.advancedHandCaughtWithDifferent3 = Integer.parseInt(basicCaughtWithDifferent3TextField.getText());
                uiScoreVariablesInput.advancedHandCaughtWithDifferent4 = Integer.parseInt(basicCaughtWithDifferent4TextField.getText());
            } else if (pickupTypeSetting == 2) {
                uiScoreVariablesInput.advancedHeadBaseScore = Integer.parseInt(basicBaseScoreTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusOrbVelocity = Integer.parseInt(basicBonusHeadTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusOrbRotation = Integer.parseInt(basicBonusFeetTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusHead = Integer.parseInt(basicBonusSliderTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusFeet = Integer.parseInt(basicBonusSpinnerTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusOrbDistance = Integer.parseInt(basicBonusOrbDistanceTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusSlider = Integer.parseInt(basicBonusOrbVelocityTextField.getText());
                uiScoreVariablesInput.advancedHeadBonusSpinner = Integer.parseInt(basicBonusOrbRotationTextField.getText());
                uiScoreVariablesInput.advancedHeadCaughtWithDifferent1 = Integer.parseInt(basicCaughtWithDifferent1TextField.getText());
                uiScoreVariablesInput.advancedHeadCaughtWithDifferent2 = Integer.parseInt(basicCaughtWithDifferent2TextField.getText());
                uiScoreVariablesInput.advancedHeadCaughtWithDifferent3 = Integer.parseInt(basicCaughtWithDifferent3TextField.getText());
                uiScoreVariablesInput.advancedHeadCaughtWithDifferent4 = Integer.parseInt(basicCaughtWithDifferent4TextField.getText());
            } else if (pickupTypeSetting == 3) {
                uiScoreVariablesInput.advancedFootBaseScore = Integer.parseInt(basicBaseScoreTextField.getText());
                uiScoreVariablesInput.advancedFootBonusOrbVelocity = Integer.parseInt(basicBonusHeadTextField.getText());
                uiScoreVariablesInput.advancedFootBonusOrbRotation = Integer.parseInt(basicBonusFeetTextField.getText());
                uiScoreVariablesInput.advancedFootBonusHead = Integer.parseInt(basicBonusSliderTextField.getText());
                uiScoreVariablesInput.advancedFootBonusFeet = Integer.parseInt(basicBonusSpinnerTextField.getText());
                uiScoreVariablesInput.advancedFootBonusOrbDistance = Integer.parseInt(basicBonusOrbDistanceTextField.getText());
                uiScoreVariablesInput.advancedFootBonusSlider = Integer.parseInt(basicBonusOrbVelocityTextField.getText());
                uiScoreVariablesInput.advancedFootBonusSpinner = Integer.parseInt(basicBonusOrbRotationTextField.getText());
                uiScoreVariablesInput.advancedFootCaughtWithDifferent1 = Integer.parseInt(basicCaughtWithDifferent1TextField.getText());
                uiScoreVariablesInput.advancedFootCaughtWithDifferent2 = Integer.parseInt(basicCaughtWithDifferent2TextField.getText());
                uiScoreVariablesInput.advancedFootCaughtWithDifferent3 = Integer.parseInt(basicCaughtWithDifferent3TextField.getText());
                uiScoreVariablesInput.advancedFootCaughtWithDifferent4 = Integer.parseInt(basicCaughtWithDifferent4TextField.getText());
            }

            setCurrentMathSettingsList();
            saveToTxt();
            return true;
        } else {
            return false;
        }

    }  // done
    private void putInUiAdvancedUiScoreVariablesInput() {

        if (pickupTypeSetting == -1) {
            basicBaseScoreTextField.setText(String.valueOf(uiScoreVariablesInput.basicBaseScore));
            basicBonusHeadTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusOrbVelocity));
            basicBonusFeetTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusOrbRotation));
            basicBonusSliderTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusHead));
            basicBonusSpinnerTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusFeet));
            basicBonusOrbDistanceTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusOrbDistance));
            basicBonusOrbVelocityTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusSlider));
            basicBonusOrbRotationTextField.setText(String.valueOf(uiScoreVariablesInput.basicBonusSpinner));
            basicCaughtWithDifferent1TextField.setText(String.valueOf(uiScoreVariablesInput.basicCaughtWithDifferent1));
            basicCaughtWithDifferent2TextField.setText(String.valueOf(uiScoreVariablesInput.basicCaughtWithDifferent2));
            basicCaughtWithDifferent3TextField.setText(String.valueOf(uiScoreVariablesInput.basicCaughtWithDifferent3));
            basicCaughtWithDifferent4TextField.setText(String.valueOf(uiScoreVariablesInput.basicCaughtWithDifferent4));
        } else if (pickupTypeSetting == 0) {
            settingsPickupTypeSettingText.setText("Any");
            basicBaseScoreTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBaseScore));
            basicBonusHeadTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusOrbVelocity));
            basicBonusFeetTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusOrbRotation));
            basicBonusSliderTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusHead));
            basicBonusSpinnerTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusFeet));
            basicBonusOrbDistanceTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusOrbDistance));
            basicBonusOrbVelocityTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusSlider));
            basicBonusOrbRotationTextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyBonusSpinner));
            basicCaughtWithDifferent1TextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyCaughtWithDifferent1));
            basicCaughtWithDifferent2TextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyCaughtWithDifferent2));
            basicCaughtWithDifferent3TextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyCaughtWithDifferent3));
            basicCaughtWithDifferent4TextField.setText(String.valueOf(uiScoreVariablesInput.advancedAnyCaughtWithDifferent4));
        } else if (pickupTypeSetting == 1) {
            settingsPickupTypeSettingText.setText("Hand");
            basicBaseScoreTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBaseScore));
            basicBonusHeadTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusOrbVelocity));
            basicBonusFeetTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusOrbRotation));
            basicBonusSliderTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusHead));
            basicBonusSpinnerTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusFeet));
            basicBonusOrbDistanceTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusOrbDistance));
            basicBonusOrbVelocityTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusSlider));
            basicBonusOrbRotationTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandBonusSpinner));
            basicCaughtWithDifferent1TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandCaughtWithDifferent1));
            basicCaughtWithDifferent2TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandCaughtWithDifferent2));
            basicCaughtWithDifferent3TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandCaughtWithDifferent3));
            basicCaughtWithDifferent4TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHandCaughtWithDifferent4));
        } else if (pickupTypeSetting == 2) {
            settingsPickupTypeSettingText.setText("Head");
            basicBaseScoreTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBaseScore));
            basicBonusHeadTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusOrbVelocity));
            basicBonusFeetTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusOrbRotation));
            basicBonusSliderTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusHead));
            basicBonusSpinnerTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusFeet));
            basicBonusOrbDistanceTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusOrbDistance));
            basicBonusOrbVelocityTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusSlider));
            basicBonusOrbRotationTextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadBonusSpinner));
            basicCaughtWithDifferent1TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadCaughtWithDifferent1));
            basicCaughtWithDifferent2TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadCaughtWithDifferent2));
            basicCaughtWithDifferent3TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadCaughtWithDifferent3));
            basicCaughtWithDifferent4TextField.setText(String.valueOf(uiScoreVariablesInput.advancedHeadCaughtWithDifferent4));
        } else if (pickupTypeSetting == 3) {
            settingsPickupTypeSettingText.setText("Foot");
            basicBaseScoreTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBaseScore));
            basicBonusHeadTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusOrbVelocity));
            basicBonusFeetTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusOrbRotation));
            basicBonusSliderTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusHead));
            basicBonusSpinnerTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusFeet));
            basicBonusOrbDistanceTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusOrbDistance));
            basicBonusOrbVelocityTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusSlider));
            basicBonusOrbRotationTextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootBonusSpinner));
            basicCaughtWithDifferent1TextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootCaughtWithDifferent1));
            basicCaughtWithDifferent2TextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootCaughtWithDifferent2));
            basicCaughtWithDifferent3TextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootCaughtWithDifferent3));
            basicCaughtWithDifferent4TextField.setText(String.valueOf(uiScoreVariablesInput.advancedFootCaughtWithDifferent4));
        }

        calculatingList = getCurrentMathSettingsList();
        updateBasicMathSettingJList();

    } // done

    private void saveToTxt() {
        File file = new File(txtFileName);

        String tempListString = "";

        for (int i = 0; i < mathSettingsListClass.anyMathSettingList.size(); i++) {
            tempListString += "mathSettingsListClass.anyMathSettingList" + ";" + i + ";" + mathSettingsListClass.anyMathSettingList.get(i) + System.lineSeparator();
        }
        for (int i = 0; i < mathSettingsListClass.handMathSettingList.size(); i++) {
            tempListString += "mathSettingsListClass.handMathSettingList" + ";" + i + ";" + mathSettingsListClass.handMathSettingList.get(i) + System.lineSeparator();
        }
        for (int i = 0; i < mathSettingsListClass.headMathSettingList.size(); i++) {
            tempListString += "mathSettingsListClass.headMathSettingList" + ";" + i + ";" + mathSettingsListClass.headMathSettingList.get(i) + System.lineSeparator();
        }
        for (int i = 0; i < mathSettingsListClass.footMathSettingList.size(); i++) {
            tempListString += "mathSettingsListClass.footMathSettingList" + ";" + i + ";" + mathSettingsListClass.footMathSettingList.get(i) + System.lineSeparator();
        }
        for (int i = 0; i < mathSettingsListClass.basicMathSettingsList.size(); i++) {
            tempListString += "mathSettingsListClass.basicMathSettingsList" + ";" + i + ";" + mathSettingsListClass.basicMathSettingsList.get(i) + System.lineSeparator();
        }


        String fileString = tempListString

                + "advancedAnyBaseScore" + ";" + uiScoreVariablesInput.advancedAnyBaseScore + System.lineSeparator()
                + "advancedAnyBonusOrbVelocity" + ";" + uiScoreVariablesInput.advancedAnyBonusOrbVelocity + System.lineSeparator()
                + "advancedAnyBonusOrbRotation" + ";" + uiScoreVariablesInput.advancedAnyBonusOrbRotation + System.lineSeparator()
                + "advancedAnyBonusHead" + ";" + uiScoreVariablesInput.advancedAnyBonusHead + System.lineSeparator()
                + "advancedAnyBonusFeet" + ";" + uiScoreVariablesInput.advancedAnyBonusFeet + System.lineSeparator()
                + "advancedAnyBonusOrbDistance" + ";" + uiScoreVariablesInput.advancedAnyBonusOrbDistance + System.lineSeparator()
                + "advancedAnyBonusSlider" + ";" + uiScoreVariablesInput.advancedAnyBonusSlider + System.lineSeparator()
                + "advancedAnyBonusSpinner" + ";" + uiScoreVariablesInput.advancedAnyBonusSpinner + System.lineSeparator()
                + "advancedAnyCaughtWithDifferent1" + ";" + uiScoreVariablesInput.advancedAnyCaughtWithDifferent1 + System.lineSeparator()
                + "advancedAnyCaughtWithDifferent2" + ";" + uiScoreVariablesInput.advancedAnyCaughtWithDifferent2 + System.lineSeparator()
                + "advancedAnyCaughtWithDifferent3" + ";" + uiScoreVariablesInput.advancedAnyCaughtWithDifferent3 + System.lineSeparator()
                + "advancedAnyCaughtWithDifferent4" + ";" + uiScoreVariablesInput.advancedAnyCaughtWithDifferent4 + System.lineSeparator()
                + "advancedHandBaseScore" + ";" + uiScoreVariablesInput.advancedHandBaseScore + System.lineSeparator()
                + "advancedHandBonusOrbVelocity" + ";" + uiScoreVariablesInput.advancedHandBonusOrbVelocity + System.lineSeparator()
                + "advancedHandBonusOrbRotation" + ";" + uiScoreVariablesInput.advancedHandBonusOrbRotation + System.lineSeparator()
                + "advancedHandBonusHead" + ";" + uiScoreVariablesInput.advancedHandBonusHead + System.lineSeparator()
                + "advancedHandBonusFeet" + ";" + uiScoreVariablesInput.advancedHandBonusFeet + System.lineSeparator()
                + "advancedHandBonusOrbDistance" + ";" + uiScoreVariablesInput.advancedHandBonusOrbDistance + System.lineSeparator()
                + "advancedHandBonusSlider" + ";" + uiScoreVariablesInput.advancedHandBonusSlider + System.lineSeparator()
                + "advancedHandBonusSpinner" + ";" + uiScoreVariablesInput.advancedHandBonusSpinner + System.lineSeparator()
                + "advancedHandCaughtWithDifferent1" + ";" + uiScoreVariablesInput.advancedHandCaughtWithDifferent1 + System.lineSeparator()
                + "advancedHandCaughtWithDifferent2" + ";" + uiScoreVariablesInput.advancedHandCaughtWithDifferent2 + System.lineSeparator()
                + "advancedHandCaughtWithDifferent3" + ";" + uiScoreVariablesInput.advancedHandCaughtWithDifferent3 + System.lineSeparator()
                + "advancedHandCaughtWithDifferent4" + ";" + uiScoreVariablesInput.advancedHandCaughtWithDifferent4 + System.lineSeparator()
                + "advancedHeadBaseScore" + ";" + uiScoreVariablesInput.advancedHeadBaseScore + System.lineSeparator()
                + "advancedHeadBonusOrbVelocity" + ";" + uiScoreVariablesInput.advancedHeadBonusOrbVelocity + System.lineSeparator()
                + "advancedHeadBonusOrbRotation" + ";" + uiScoreVariablesInput.advancedHeadBonusOrbRotation + System.lineSeparator()
                + "advancedHeadBonusHead" + ";" + uiScoreVariablesInput.advancedHeadBonusHead + System.lineSeparator()
                + "advancedHeadBonusFeet" + ";" + uiScoreVariablesInput.advancedHeadBonusFeet + System.lineSeparator()
                + "advancedHeadBonusOrbDistance" + ";" + uiScoreVariablesInput.advancedHeadBonusOrbDistance + System.lineSeparator()
                + "advancedHeadBonusSlider" + ";" + uiScoreVariablesInput.advancedHeadBonusSlider + System.lineSeparator()
                + "advancedHeadBonusSpinner" + ";" + uiScoreVariablesInput.advancedHeadBonusSpinner + System.lineSeparator()
                + "advancedHeadCaughtWithDifferent1" + ";" + uiScoreVariablesInput.advancedHeadCaughtWithDifferent1 + System.lineSeparator()
                + "advancedHeadCaughtWithDifferent2" + ";" + uiScoreVariablesInput.advancedHeadCaughtWithDifferent2 + System.lineSeparator()
                + "advancedHeadCaughtWithDifferent3" + ";" + uiScoreVariablesInput.advancedHeadCaughtWithDifferent3 + System.lineSeparator()
                + "advancedHeadCaughtWithDifferent4" + ";" + uiScoreVariablesInput.advancedHeadCaughtWithDifferent4 + System.lineSeparator()
                + "advancedFootBaseScore" + ";" + uiScoreVariablesInput.advancedFootBaseScore + System.lineSeparator()
                + "advancedFootBonusOrbVelocity" + ";" + uiScoreVariablesInput.advancedFootBonusOrbVelocity + System.lineSeparator()
                + "advancedFootBonusOrbRotation" + ";" + uiScoreVariablesInput.advancedFootBonusOrbRotation + System.lineSeparator()
                + "advancedFootBonusHead" + ";" + uiScoreVariablesInput.advancedFootBonusHead + System.lineSeparator()
                + "advancedFootBonusFeet" + ";" + uiScoreVariablesInput.advancedFootBonusFeet + System.lineSeparator()
                + "advancedFootBonusOrbDistance" + ";" + uiScoreVariablesInput.advancedFootBonusOrbDistance + System.lineSeparator()
                + "advancedFootBonusSlider" + ";" + uiScoreVariablesInput.advancedFootBonusSlider + System.lineSeparator()
                + "advancedFootBonusSpinner" + ";" + uiScoreVariablesInput.advancedFootBonusSpinner + System.lineSeparator()
                + "advancedFootCaughtWithDifferent1" + ";" + uiScoreVariablesInput.advancedFootCaughtWithDifferent1 + System.lineSeparator()
                + "advancedFootCaughtWithDifferent2" + ";" + uiScoreVariablesInput.advancedFootCaughtWithDifferent2 + System.lineSeparator()
                + "advancedFootCaughtWithDifferent3" + ";" + uiScoreVariablesInput.advancedFootCaughtWithDifferent3 + System.lineSeparator()
                + "advancedFootCaughtWithDifferent4" + ";" + uiScoreVariablesInput.advancedFootCaughtWithDifferent4 + System.lineSeparator()
                // basic Settings
                + "basicBaseScore" + ";" + uiScoreVariablesInput.basicBaseScore + System.lineSeparator()
                + "basicBonusOrbVelocity" + ";" + uiScoreVariablesInput.basicBonusOrbVelocity + System.lineSeparator()
                + "basicBonusOrbRotation" + ";" + uiScoreVariablesInput.basicBonusOrbRotation + System.lineSeparator()
                + "basicBonusHead" + ";" + uiScoreVariablesInput.basicBonusHead + System.lineSeparator()
                + "basicBonusFeet" + ";" + uiScoreVariablesInput.basicBonusFeet + System.lineSeparator()
                + "basicBonusOrbDistance" + ";" + uiScoreVariablesInput.basicBonusOrbDistance + System.lineSeparator()
                + "basicBonusSlider" + ";" + uiScoreVariablesInput.basicBonusSlider + System.lineSeparator()
                + "basicBonusSpinner" + ";" + uiScoreVariablesInput.basicBonusSpinner + System.lineSeparator()
                + "basicCaughtWithDifferent1" + ";" + uiScoreVariablesInput.basicCaughtWithDifferent1 + System.lineSeparator()
                + "basicCaughtWithDifferent2" + ";" + uiScoreVariablesInput.basicCaughtWithDifferent2 + System.lineSeparator()
                + "basicCaughtWithDifferent3" + ";" + uiScoreVariablesInput.basicCaughtWithDifferent3 + System.lineSeparator()
                + "basicCaughtWithDifferent4" + ";" + uiScoreVariablesInput.basicCaughtWithDifferent4;

        try {
            FileUtils.writeStringToFile(file, fileString);
        } catch (IOException e) {
            e.printStackTrace();
        }

    } // done
    private void loadFromTxt() {

        File file = new File(txtFileName);

        if (!file.exists()) {
            loadNull();
        } else {
            String fileString = "";
            try {
                fileString = FileUtils.readFileToString(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] fileStringArray = fileString.split(System.lineSeparator());

            for (String tempString : fileStringArray) {

                String[] split = tempString.split(";");

                // advanced
                if (split[0].equalsIgnoreCase("mathSettingsListClass.anyMathSettingList")) {
                    String repairString = "";
                    for (int i = 2; i < split.length; i++) {
                        if (i == 2) {
                            repairString += split[i];
                        } else {
                            repairString += ";" + split[i];
                        }
                    }
                    mathSettingsListClass.anyMathSettingList.add(repairString);
                }
                if (split[0].equalsIgnoreCase("mathSettingsListClass.handMathSettingList")) {
                    String repairString = "";
                    for (int i = 2; i < split.length; i++) {
                        if (i == 2) {
                            repairString += split[i];
                        } else {
                            repairString += ";" + split[i];
                        }
                    }
                    mathSettingsListClass.handMathSettingList.add(repairString);
                }
                if (split[0].equalsIgnoreCase("mathSettingsListClass.headMathSettingList")) {
                    String repairString = "";
                    for (int i = 2; i < split.length; i++) {
                        if (i == 2) {
                            repairString += split[i];
                        } else {
                            repairString += ";" + split[i];
                        }
                    }
                    mathSettingsListClass.headMathSettingList.add(repairString);
                }
                if (split[0].equalsIgnoreCase("mathSettingsListClass.footMathSettingList")) {
                    String repairString = "";
                    for (int i = 2; i < split.length; i++) {
                        if (i == 2) {
                            repairString += split[i];
                        } else {
                            repairString += ";" + split[i];
                        }
                    }
                    mathSettingsListClass.footMathSettingList.add(repairString);
                }
                // basic
                if (split[0].equalsIgnoreCase("mathSettingsListClass.basicMathSettingsList")) {
                    String repairString = "";
                    for (int i = 2; i < split.length; i++) {
                        if (i == 2) {
                            repairString += split[i];
                        } else {
                            repairString += ";" + split[i];
                        }
                    }
                    mathSettingsListClass.basicMathSettingsList.add(repairString);
                }

                // advanced
                if (split[0].equalsIgnoreCase("advancedAnyBaseScore")) {
                    uiScoreVariablesInput.advancedAnyBaseScore = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusOrbVelocity")) {
                    uiScoreVariablesInput.advancedAnyBonusOrbVelocity = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusOrbRotation")) {
                    uiScoreVariablesInput.advancedAnyBonusOrbRotation = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusHead")) {
                    uiScoreVariablesInput.advancedAnyBonusHead = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusFeet")) {
                    uiScoreVariablesInput.advancedAnyBonusFeet = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusOrbDistance")) {
                    uiScoreVariablesInput.advancedAnyBonusOrbDistance = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusSlider")) {
                    uiScoreVariablesInput.advancedAnyBonusSlider = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyBonusSpinner")) {
                    uiScoreVariablesInput.advancedAnyBonusSpinner = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyCaughtWithDifferent1")) {
                    uiScoreVariablesInput.advancedAnyCaughtWithDifferent1 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyCaughtWithDifferent2")) {
                    uiScoreVariablesInput.advancedAnyCaughtWithDifferent2 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyCaughtWithDifferent3")) {
                    uiScoreVariablesInput.advancedAnyCaughtWithDifferent3 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedAnyCaughtWithDifferent4")) {
                    uiScoreVariablesInput.advancedAnyCaughtWithDifferent4 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBaseScore")) {
                    uiScoreVariablesInput.advancedHandBaseScore = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusOrbVelocity")) {
                    uiScoreVariablesInput.advancedHandBonusOrbVelocity = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusOrbRotation")) {
                    uiScoreVariablesInput.advancedHandBonusOrbRotation = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusHead")) {
                    uiScoreVariablesInput.advancedHandBonusHead = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusFeet")) {
                    uiScoreVariablesInput.advancedHandBonusFeet = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusOrbDistance")) {
                    uiScoreVariablesInput.advancedHandBonusOrbDistance = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusSlider")) {
                    uiScoreVariablesInput.advancedHandBonusSlider = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandBonusSpinner")) {
                    uiScoreVariablesInput.advancedHandBonusSpinner = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandCaughtWithDifferent1")) {
                    uiScoreVariablesInput.advancedHandCaughtWithDifferent1 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandCaughtWithDifferent2")) {
                    uiScoreVariablesInput.advancedHandCaughtWithDifferent2 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandCaughtWithDifferent3")) {
                    uiScoreVariablesInput.advancedHandCaughtWithDifferent3 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHandCaughtWithDifferent4")) {
                    uiScoreVariablesInput.advancedHandCaughtWithDifferent4 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBaseScore")) {
                    uiScoreVariablesInput.advancedHeadBaseScore = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusOrbVelocity")) {
                    uiScoreVariablesInput.advancedHeadBonusOrbVelocity = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusOrbRotation")) {
                    uiScoreVariablesInput.advancedHeadBonusOrbRotation = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusHead")) {
                    uiScoreVariablesInput.advancedHeadBonusHead = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusFeet")) {
                    uiScoreVariablesInput.advancedHeadBonusFeet = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusOrbDistance")) {
                    uiScoreVariablesInput.advancedHeadBonusOrbDistance = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusSlider")) {
                    uiScoreVariablesInput.advancedHeadBonusSlider = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadBonusSpinner")) {
                    uiScoreVariablesInput.advancedHeadBonusSpinner = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadCaughtWithDifferent1")) {
                    uiScoreVariablesInput.advancedHeadCaughtWithDifferent1 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadCaughtWithDifferent2")) {
                    uiScoreVariablesInput.advancedHeadCaughtWithDifferent2 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadCaughtWithDifferent3")) {
                    uiScoreVariablesInput.advancedHeadCaughtWithDifferent3 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedHeadCaughtWithDifferent4")) {
                    uiScoreVariablesInput.advancedHeadCaughtWithDifferent4 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBaseScore")) {
                    uiScoreVariablesInput.advancedFootBaseScore = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusOrbVelocity")) {
                    uiScoreVariablesInput.advancedFootBonusOrbVelocity = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusOrbRotation")) {
                    uiScoreVariablesInput.advancedFootBonusOrbRotation = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusHead")) {
                    uiScoreVariablesInput.advancedFootBonusHead = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusFeet")) {
                    uiScoreVariablesInput.advancedFootBonusFeet = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusOrbDistance")) {
                    uiScoreVariablesInput.advancedFootBonusOrbDistance = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusSlider")) {
                    uiScoreVariablesInput.advancedFootBonusSlider = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootBonusSpinner")) {
                    uiScoreVariablesInput.advancedFootBonusSpinner = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootCaughtWithDifferent1")) {
                    uiScoreVariablesInput.advancedFootCaughtWithDifferent1 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootCaughtWithDifferent2")) {
                    uiScoreVariablesInput.advancedFootCaughtWithDifferent2 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootCaughtWithDifferent3")) {
                    uiScoreVariablesInput.advancedFootCaughtWithDifferent3 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("advancedFootCaughtWithDifferent4")) {
                    uiScoreVariablesInput.advancedFootCaughtWithDifferent4 = Integer.parseInt(split[1]);
                }
                /// basic
                if (split[0].equalsIgnoreCase("basicBaseScore")) {
                    uiScoreVariablesInput.basicBaseScore = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusOrbVelocity")) {
                    uiScoreVariablesInput.basicBonusOrbVelocity = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusOrbRotation")) {
                    uiScoreVariablesInput.basicBonusOrbRotation = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusHead")) {
                    uiScoreVariablesInput.basicBonusHead = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusFeet")) {
                    uiScoreVariablesInput.basicBonusFeet = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusOrbDistance")) {
                    uiScoreVariablesInput.basicBonusOrbDistance = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusSlider")) {
                    uiScoreVariablesInput.basicBonusSlider = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicBonusSpinner")) {
                    uiScoreVariablesInput.basicBonusSpinner = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicCaughtWithDifferent1")) {
                    uiScoreVariablesInput.basicCaughtWithDifferent1 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicCaughtWithDifferent2")) {
                    uiScoreVariablesInput.basicCaughtWithDifferent2 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicCaughtWithDifferent3")) {
                    uiScoreVariablesInput.basicCaughtWithDifferent3 = Integer.parseInt(split[1]);
                }
                if (split[0].equalsIgnoreCase("basicCaughtWithDifferent4")) {
                    uiScoreVariablesInput.basicCaughtWithDifferent4 = Integer.parseInt(split[1]);
                }


            }

        }

    } // done
    private void loadNull() {

        UiScoreVariablesInput tempUiScoreVariablesInput = new UiScoreVariablesInput();

        // Any
        tempUiScoreVariablesInput.advancedAnyBaseScore = 0;
        tempUiScoreVariablesInput.advancedAnyBonusOrbVelocity = 0;
        tempUiScoreVariablesInput.advancedAnyBonusOrbRotation = 0;
        tempUiScoreVariablesInput.advancedAnyBonusHead = 0;
        tempUiScoreVariablesInput.advancedAnyBonusFeet = 0;
        tempUiScoreVariablesInput.advancedAnyBonusOrbDistance = 0;
        tempUiScoreVariablesInput.advancedAnyBonusSlider = 0;
        tempUiScoreVariablesInput.advancedAnyBonusSpinner = 0;
        tempUiScoreVariablesInput.advancedAnyCaughtWithDifferent1 = 0;
        tempUiScoreVariablesInput.advancedAnyCaughtWithDifferent2 = 0;
        tempUiScoreVariablesInput.advancedAnyCaughtWithDifferent3 = 0;
        tempUiScoreVariablesInput.advancedAnyCaughtWithDifferent4 = 0;
        // Hand
        tempUiScoreVariablesInput.advancedHandBaseScore = 0;
        tempUiScoreVariablesInput.advancedHandBonusOrbVelocity = 0;
        tempUiScoreVariablesInput.advancedHandBonusOrbRotation = 0;
        tempUiScoreVariablesInput.advancedHandBonusHead = 0;
        tempUiScoreVariablesInput.advancedHandBonusFeet = 0;
        tempUiScoreVariablesInput.advancedHandBonusOrbDistance = 0;
        tempUiScoreVariablesInput.advancedHandBonusSlider = 0;
        tempUiScoreVariablesInput.advancedHandBonusSpinner = 0;
        tempUiScoreVariablesInput.advancedHandCaughtWithDifferent1 = 0;
        tempUiScoreVariablesInput.advancedHandCaughtWithDifferent2 = 0;
        tempUiScoreVariablesInput.advancedHandCaughtWithDifferent3 = 0;
        tempUiScoreVariablesInput.advancedHandCaughtWithDifferent4 = 0;
        // Head
        tempUiScoreVariablesInput.advancedHeadBaseScore = 0;
        tempUiScoreVariablesInput.advancedHeadBonusOrbVelocity = 0;
        tempUiScoreVariablesInput.advancedHeadBonusOrbRotation = 0;
        tempUiScoreVariablesInput.advancedHeadBonusHead = 0;
        tempUiScoreVariablesInput.advancedHeadBonusFeet = 0;
        tempUiScoreVariablesInput.advancedHeadBonusOrbDistance = 0;
        tempUiScoreVariablesInput.advancedHeadBonusSlider = 0;
        tempUiScoreVariablesInput.advancedHeadBonusSpinner = 0;
        tempUiScoreVariablesInput.advancedHeadCaughtWithDifferent1 = 0;
        tempUiScoreVariablesInput.advancedHeadCaughtWithDifferent2 = 0;
        tempUiScoreVariablesInput.advancedHeadCaughtWithDifferent3 = 0;
        tempUiScoreVariablesInput.advancedHeadCaughtWithDifferent4 = 0;
        // Foot
        tempUiScoreVariablesInput.advancedFootBaseScore = 0;
        tempUiScoreVariablesInput.advancedFootBonusOrbVelocity = 0;
        tempUiScoreVariablesInput.advancedFootBonusOrbRotation = 0;
        tempUiScoreVariablesInput.advancedFootBonusHead = 0;
        tempUiScoreVariablesInput.advancedFootBonusFeet = 0;
        tempUiScoreVariablesInput.advancedFootBonusOrbDistance = 0;
        tempUiScoreVariablesInput.advancedFootBonusSlider = 0;
        tempUiScoreVariablesInput.advancedFootBonusSpinner = 0;
        tempUiScoreVariablesInput.advancedFootCaughtWithDifferent1 = 0;
        tempUiScoreVariablesInput.advancedFootCaughtWithDifferent2 = 0;
        tempUiScoreVariablesInput.advancedFootCaughtWithDifferent3 = 0;
        tempUiScoreVariablesInput.advancedFootCaughtWithDifferent4 = 0;
        // basic
        tempUiScoreVariablesInput.basicBaseScore = 0;
        tempUiScoreVariablesInput.basicBonusOrbVelocity = 0;
        tempUiScoreVariablesInput.basicBonusOrbRotation = 0;
        tempUiScoreVariablesInput.basicBonusHead = 0;
        tempUiScoreVariablesInput.basicBonusFeet = 0;
        tempUiScoreVariablesInput.basicBonusOrbDistance = 0;
        tempUiScoreVariablesInput.basicBonusSlider = 0;
        tempUiScoreVariablesInput.basicBonusSpinner = 0;
        tempUiScoreVariablesInput.basicCaughtWithDifferent1 = 0;
        tempUiScoreVariablesInput.basicCaughtWithDifferent2 = 0;
        tempUiScoreVariablesInput.basicCaughtWithDifferent3 = 0;
        tempUiScoreVariablesInput.basicCaughtWithDifferent4 = 0;

        uiScoreVariablesInput = tempUiScoreVariablesInput;

    } // done

    private List<String> getCurrentMathSettingsList() {

        if (pickupTypeSetting == -1) {
            return mathSettingsListClass.basicMathSettingsList;

        } else if (pickupTypeSetting == 0) {
            return mathSettingsListClass.anyMathSettingList;

        } else if (pickupTypeSetting == 1) {
            return mathSettingsListClass.handMathSettingList;

        } else if (pickupTypeSetting == 2) {
            return mathSettingsListClass.headMathSettingList;

        } else if (pickupTypeSetting == 3) {
            return mathSettingsListClass.footMathSettingList;

        } else {
            return null;
        }


    } // done
    private void setCurrentMathSettingsList() {

        if (pickupTypeSetting == -1) {
            mathSettingsListClass.basicMathSettingsList = calculatingList;

        } else if (pickupTypeSetting == 0) {
            mathSettingsListClass.anyMathSettingList = calculatingList;

        } else if (pickupTypeSetting == 1) {
            mathSettingsListClass.handMathSettingList = calculatingList;

        } else if (pickupTypeSetting == 2) {
            mathSettingsListClass.headMathSettingList = calculatingList;

        } else if (pickupTypeSetting == 3) {
            mathSettingsListClass.footMathSettingList = calculatingList;

        }


    } // done

    private void updateBasicMathSettingJList() {
        DefaultListModel tempMathListModel = new DefaultListModel();

        for (String tempString : calculatingList) {
            tempMathListModel.addElement(tempString.replace(";", " "));
        }

        basicMathSettingJList.setModel(tempMathListModel);
    }

    private String formatScore(long scorePoints) {
        char[] tempString = Long.toString(scorePoints).toCharArray();
        String scorePointsString = "";
        int count = 1;
        for (int i = tempString.length - 1; i >= 0; i--) {
            scorePointsString = Character.toString(tempString[i]) + scorePointsString;
            if (count == 3) {
                count = 0;
                scorePointsString = "." + scorePointsString;
            }
            count++;
        }
        if (scorePointsString.startsWith(".")) {
            scorePointsString = scorePointsString.substring(1);
        }
        return scorePointsString;
    }

    private boolean setDefaultSettings(){
        File file = new File(txtFileNameDefault);
        if(file.exists()) {

            if (isUiUiScoreVariablesAllNumbers()) {
                basicBaseScoreTextField.setEnabled(false);
                basicBonusOrbRotationTextField.setEnabled(false);
                basicBonusFeetTextField.setEnabled(false);
                basicBonusSliderTextField.setEnabled(false);
                basicBonusOrbVelocityTextField.setEnabled(false);
                basicBonusOrbDistanceTextField.setEnabled(false);
                basicBonusSpinnerTextField.setEnabled(false);
                basicBonusHeadTextField.setEnabled(false);
                basicCaughtWithDifferent1TextField.setEnabled(false);
                basicCaughtWithDifferent2TextField.setEnabled(false);
                basicCaughtWithDifferent3TextField.setEnabled(false);
                basicCaughtWithDifferent4TextField.setEnabled(false);

                basicAddMathSettingButton.setEnabled(false);
                basicDeleteMathSettingButton.setEnabled(false);


                saveToTxt();

                txtFileName = txtFileNameDefault;

                loadFromTxt();
                putInUiAdvancedUiScoreVariablesInput();

            }
            return true;
        } else {
            infoText.setText("You need a HolodanceScoreCalculatorDataDefault.Txt file.");
            return false;
        }

    }

    private void setCustomSettings(){

            basicBaseScoreTextField.setEnabled(true);
            basicBonusOrbRotationTextField.setEnabled(true);
            basicBonusFeetTextField.setEnabled(true);
            basicBonusSliderTextField.setEnabled(true);
            basicBonusOrbVelocityTextField.setEnabled(true);
            basicBonusOrbDistanceTextField.setEnabled(true);
            basicBonusSpinnerTextField.setEnabled(true);
            basicBonusHeadTextField.setEnabled(true);
            basicCaughtWithDifferent1TextField.setEnabled(true);
            basicCaughtWithDifferent2TextField.setEnabled(true);
            basicCaughtWithDifferent3TextField.setEnabled(true);
            basicCaughtWithDifferent4TextField.setEnabled(true);

            basicAddMathSettingButton.setEnabled(true);
            basicDeleteMathSettingButton.setEnabled(true);

            txtFileName = txtFileNameCustom;

            loadFromTxt();
            putInUiAdvancedUiScoreVariablesInput();


    }

    private UI getOwnClass(){
        return this;
    }



    public void setReplayJson(ReplayJson replayJson) {
        this.replayJson = replayJson;


        loadReplayFileButton.setEnabled(false);
        loadReplayButton.setEnabled(false);
        Thread t = new Thread() {

            public void run() {


                DefaultListModel listModel = new DefaultListModel();

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");


                for (ReplayData replayData : replayJson.getReplayData()) {
                    listModel.addElement("Player Name: " + replayData.getPlayerSelf().getNickname() + "; Played at: " + dateFormat.format(new Date((replayData.getPlayedDate() - 621355968000000000L) / 10000)));

                }
                replayList.setModel(listModel);


                if (replayJson.getReplayData().size() > 0) {
                    replayList.setSelectedIndex(0);
                    loadReplayButton.setEnabled(true);
                    infoText.setText("Select Replay.");
                }


                loadReplayFileButton.setEnabled(true);
            }
        };
        t.start();

    }


}
