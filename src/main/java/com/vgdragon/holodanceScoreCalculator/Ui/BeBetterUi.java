package com.vgdragon.holodanceScoreCalculator.Ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vgdragon.holodanceScoreCalculator.Data;
import com.vgdragon.holodanceScoreCalculator.Graphics.Stats.BeBetterStats;
import com.vgdragon.holodanceScoreCalculator.Graphics.StatsGraphic;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses.RecordedEvent;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses.ReplayData;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayJson;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BeBetterUi {


    private JPanel jpanel;
    private JButton loadReplayFileButton;
    private JList replayList;
    private JTextArea tipTextArea;
    private JButton loadReplayButton;
    private JButton button1;
    private JPanel graphicPanel;
    private static JFrame frame;


    private ReplayJson replayJson;
    private ReplayData replayData;
    private List<RecordedEvent> recordedEventsList;
    Data data = new Data();

    public static void ui() {
        frame = new JFrame("Be better.");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new BeBetterUi().jpanel);
        frame.setSize(new Dimension(400,500));
        frame.setVisible(true);


    }

    public BeBetterUi() {
        graphicPanel.setVisible(false);

        try {
            data = new ObjectMapper().readValue(new File(data.getDataFolder() + data.getDataFileName()), Data.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        loadReplayFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UISetBeatMap.ui(BeBetterUi.this, data);

            }
        });

        loadReplayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadReplayButton.setEnabled(false);
                replayData = replayJson.getReplayData().get(replayList.getSelectedIndex());


                recordedEventsList = new ArrayList<>();

                Thread t = new Thread() {
                    public void run() {

                        recordedEventsList = replayData.getRecordedEvents();

                        loadReplayButton.setEnabled(true);
                        //infoText.setText("Ready.");
                        getReplayInfo();
                    }

                };

                t.start();


            }
        });


    }


    public void setReplayJson(ReplayJson replayJson) {
        this.replayJson = replayJson;


        loadReplayFileButton.setEnabled(false);
        loadReplayButton.setEnabled(false);
        Thread t = new Thread() {

            public void run() {


                DefaultListModel listModel = new DefaultListModel();

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");


                for (ReplayData replayData : replayJson.getReplayData()) {
                    listModel.addElement("Player Name: " + replayData.getPlayerSelf().getNickname() + "; Played at: " + dateFormat.format(new Date((replayData.getPlayedDate() - 621355968000000000L) / 10000)));

                }
                replayList.setModel(listModel);


                if (replayJson.getReplayData().size() > 0) {
                    replayList.setSelectedIndex(0);
                    loadReplayButton.setEnabled(true);
                    //infoText.setText("Select Replay.");
                }


                loadReplayFileButton.setEnabled(true);
            }
        };
        t.start();

    }

    private void getReplayInfo() {

        int orbCount = 0;
        int missedOrbCount = 0;
        int toEarlyOrb = 0;
        int toLaitOrb = 0;

        int sliderCount = 0;
        int missedSliderCount = 0;
        int maxPointsSliderCount = 0;
        List<Integer> sliderPointPercentLost = new ArrayList<>();
        int sliderNotCompletedPercent = 0;

        int spinnerCount = 0;
        int missedSpinnerCount = 0;
        int maxPointsSpinnerCount = 0;
        List<Integer> spinnerPointPercentLost = new ArrayList<>();
        int spinnerNotCompletedPercent = 0;


        for (RecordedEvent recordedEvent : recordedEventsList) {
            if (recordedEvent.getTouchType() == 0) {
                orbCount++;
                if (recordedEvent.isCaught()) {

                } else {
                    missedOrbCount++;
                }
            } else if (recordedEvent.getTouchType() == 1) {
                sliderCount++;
                if (recordedEvent.isCaught()) {
                    if ((recordedEvent.getBonusSlider() * recordedEvent.getCurrentMultiplier()) == recordedEvent.getActualBonusSlider()) {
                        maxPointsSliderCount++;
                    }// else {
                        Double tempPecent = ((double) (100 / (double) (recordedEvent.getBonusSlider() * recordedEvent.getCurrentMultiplier())) * recordedEvent.getActualBonusSlider());
                        sliderPointPercentLost.add(Integer.valueOf(tempPecent.intValue()));
                    //}
                } else {
                    missedSliderCount++;
                }
            } else if (recordedEvent.getTouchType() == 3) {
                spinnerCount++;
                if (recordedEvent.isCaught()) {
                    if ((recordedEvent.getBonusSpinner() * recordedEvent.getCurrentMultiplier()) == recordedEvent.getActualBonusSpinner()) {
                        maxPointsSpinnerCount++;
                    }// else {
                        int tempPecent = (100 / (recordedEvent.getBonusSpinner() * recordedEvent.getCurrentMultiplier())) * recordedEvent.getActualBonusSpinner();
                        spinnerPointPercentLost.add(tempPecent);
                    //}
                } else {
                    missedSpinnerCount++;
                }


            }
        }

        String tipText = "" +
                "Missed Orbs: " + missedOrbCount + " from " + orbCount + " (Red)"
                + "\nMissed Slider: " + missedSliderCount + " from " + sliderCount + " (Blue)"
                + "\nMissed Spinner " + missedSpinnerCount + " from " + spinnerCount + " (Yellow)"
                + "\n";


        Double tempDouble = 0D;

        for (int tempInt : sliderPointPercentLost) {
            tempDouble += tempInt;
        }
        tempDouble /= sliderPointPercentLost.size();

        tipText += "\nYour average Slider Score" + //, if you don't get full points or missed it" +
                "\n is " + (tempDouble.intValue()) + " percent " + " (Green)" +
                "\n";
        sliderNotCompletedPercent = tempDouble.intValue();


        tempDouble = 0D;

        for (int tempInt : spinnerPointPercentLost) {
            tempDouble += tempInt;
        }
        tempDouble /= spinnerPointPercentLost.size();

        tipText += "\nYour average Spinner Score" + //, if you don't get full points or missed it" +
                "\n is " + (tempDouble.intValue()) + " percent"  + " (Magenta)" +
                "\n";
        spinnerNotCompletedPercent = tempDouble.intValue();


        BeBetterStats beBetterStats = new BeBetterStats();


        beBetterStats.setOrbCount(orbCount);
        beBetterStats.setMissedOrbCount(missedOrbCount);

        beBetterStats.setSliderCount(sliderCount);
        beBetterStats.setMissedSliderCount(missedSliderCount);
        beBetterStats.setMaxPointsSliderCount(maxPointsSliderCount);
        beBetterStats.setSliderPointPercentLost(sliderPointPercentLost);
        beBetterStats.setSliderNotCompletedPercent(sliderNotCompletedPercent);

        beBetterStats.setSpinnerCount(spinnerCount);
        beBetterStats.setMissedSpinnerCount(missedSpinnerCount);
        beBetterStats.setMaxPointsSpinnerCount(maxPointsSpinnerCount);
        beBetterStats.setSpinnerPointPercentLost(spinnerPointPercentLost);
        beBetterStats.setSpinnerNotCompletedPercent(spinnerNotCompletedPercent);

        StatsGraphic graphics = new StatsGraphic();





        graphics.setMinimumSize(new Dimension(400,120));
        //graphics.setLayout(new BoxLayout(graphicPanel, BoxLayout.PAGE_AXIS));

        graphics.setVisible(true);

        this.graphicPanel.add(graphics, new com.intellij.uiDesigner.core.GridConstraints());
        this.graphicPanel.setVisible(true);

        graphics.setBeBetterStats(beBetterStats);
        frame.setSize(new Dimension(750,500));

        System.out.println("stop");
        tipTextArea.setText("");
        tipTextArea.append(tipText);


    }


}
