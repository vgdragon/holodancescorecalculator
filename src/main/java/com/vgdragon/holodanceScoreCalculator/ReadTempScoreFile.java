package com.vgdragon.holodanceScoreCalculator;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class ReadTempScoreFile {

    public void readTempScoreFile(String fileName, String outputFileName){
        File file = new File(fileName);

        String holodanceFile = null;
        try {
            holodanceFile = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        char[] holodanceFileChars = holodanceFile.toCharArray();

        String outputString = "";
        int tabbingSpace = 0;
        StringBuilder stringBuilder = new StringBuilder();


        for(int i = 0; i < holodanceFileChars.length; i++){
            if(holodanceFileChars[i] == '{'){

                stringBuilder.append('\n');

                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }
                tabbingSpace++;
                stringBuilder.append(holodanceFileChars[i]).append('\n');
                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }
            } else if(holodanceFileChars[i] == '}'){

                tabbingSpace--;
                stringBuilder.append('\n');

                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }

                stringBuilder.append(holodanceFileChars[i]).append('\n');
                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }



            } else if(holodanceFileChars[i] == '['){

                stringBuilder.append('\n');

                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }
                tabbingSpace++;
                stringBuilder.append(holodanceFileChars[i]).append('\n');
                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }
            } else if(holodanceFileChars[i] == ']'){

                tabbingSpace--;
                stringBuilder.append('\n');

                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }

                stringBuilder.append(holodanceFileChars[i]).append('\n');
                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }



            } else if(holodanceFileChars[i] == ','){

                stringBuilder.append(holodanceFileChars[i]).append('\n');
                for(int ii = 0; ii < tabbingSpace; ii++){
                    stringBuilder.append("\t");
                }

            } else {
                //outputString = outputString + holodanceFileChars[i];

                stringBuilder.append(holodanceFileChars[i]);
            }
        }

        outputString = stringBuilder.toString();

        File outputFile = new File(outputFileName);

        try {
            FileUtils.writeStringToFile(outputFile,outputString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
