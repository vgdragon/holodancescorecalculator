package com.vgdragon.holodanceScoreCalculator.TxT;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TxtWriter {

    public TxtWriter(List<String> textList, String fileName) throws IOException {

        String inTxt = "";

        for (int i = 0; i < textList.size(); i ++){
            if(!textList.get(i).equalsIgnoreCase("")) {
                if (inTxt.equalsIgnoreCase("")) {
                    inTxt = textList.get(i);
                } else {
                    inTxt = inTxt + System.lineSeparator() + textList.get(i);
                }
            }
        }
        String filePath = fileName;

        File file = new File(filePath);

        FileUtils.writeStringToFile(file, inTxt,"UTF-8");





    }


}
