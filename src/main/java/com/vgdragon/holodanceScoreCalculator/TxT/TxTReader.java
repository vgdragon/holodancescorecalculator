package com.vgdragon.holodanceScoreCalculator.TxT;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TxTReader {

    public List<String> txtReader(String fileName) throws IOException {
        List<String> returnList = new ArrayList<>();

        String filePath = fileName;

        File file = new File(filePath);

        if(!file.exists()){
            file.createNewFile();
        }

        String fileText = FileUtils.readFileToString(file,"UTF-8");


        if(fileText != null) {

            String[] tempArray = fileText.split(System.lineSeparator());
            for(int i = 0; i < tempArray.length; i ++){
                tempArray[i] = tempArray[i].trim();
                if(!tempArray[i].equalsIgnoreCase("")){
                    returnList.add(tempArray[i]);


                }
            }

        }




        return returnList;
    }
}
