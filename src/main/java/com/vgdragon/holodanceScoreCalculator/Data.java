package com.vgdragon.holodanceScoreCalculator;

public class Data {


    private String dataFolder = "";
    private String holodanceFolder;
    private String dataFileName = "Data.json";

    public String getDataFolder() {
        return "";
    }
    public String getDataFileName(){
        return dataFileName;
    }

    public String getHolodanceFolder() {
        return holodanceFolder;
    }

    public void setHolodanceFolder(String holodanceFolder){
        this.holodanceFolder = holodanceFolder;
    }
}
