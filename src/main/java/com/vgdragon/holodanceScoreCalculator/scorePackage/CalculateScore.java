package com.vgdragon.holodanceScoreCalculator.scorePackage;

import com.vgdragon.holodanceScoreCalculator.scorePackage.Settings.MathSettingsListClass;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses.RecordedEvent;

import javax.swing.*;
import java.util.List;

public class CalculateScore {

    private int mathSettingsListCounter;
    private List<String> mathSettingsList;
    private CurrentScoreVariables currentScoreVariables =  new CurrentScoreVariables();
    private MathSettingsListClass mathSettingsListClass = new MathSettingsListClass();

    private int pickupType = 0;
    private boolean calculateScoreBasic = false;


    public long calculateScoreAdvanced(List<RecordedEvent> recordedEventsList, UiScoreVariablesInput uiScoreVariablesInput, MathSettingsListClass mathSettingsListClass, JLabel infoText){

        this.calculateScoreBasic = false;
        this.mathSettingsListClass = mathSettingsListClass;

        long scoreLong = 0L;

        for(int recordedEventsListCounter = 0; recordedEventsListCounter < recordedEventsList.size(); recordedEventsListCounter++){

            RecordedEvent recordedEvents = recordedEventsList.get(recordedEventsListCounter);
            infoText.setText("Calculate: " + (recordedEventsListCounter + 1) + " from " + recordedEventsList.size());

            if(recordedEvents.isCaught()){

                this.pickupType = recordedEvents.getPickupType();
                setCurrentScoreVariables(uiScoreVariablesInput);
                scoreLong += math(recordedEvents, true);

            }

        }

        infoText.setText("Ready.");
        return scoreLong;
    } // done

    public long calculateScoreBasic(List<RecordedEvent> recordedEventList, UiScoreVariablesInput uiScoreVariablesInput, MathSettingsListClass mathSettingsListClass, JLabel infoText){

        this.calculateScoreBasic = true;
        this.mathSettingsListClass = mathSettingsListClass;
        setCurrentScoreVariables(uiScoreVariablesInput);


        long scoreLong = 0L;

        for(int recordedEventsListCounter = 0; recordedEventsListCounter < recordedEventList.size(); recordedEventsListCounter++) {

            RecordedEvent recordedEvent = recordedEventList.get(recordedEventsListCounter);
            infoText.setText("Calculate: " + (recordedEventsListCounter + 1) + " from " + recordedEventList.size());

            if(recordedEvent.isCaught()) {
                scoreLong += math(recordedEvent, true);

            }

        }

        infoText.setText("Ready.");
        return scoreLong;
    } // done



    public double math(RecordedEvent recordedEvents, boolean fistTry){
        double returnDouble = 0;
        if(!fistTry){
            mathSettingsListCounter++;
        } else {
            mathSettingsListCounter = 0;
        }

        for (; mathSettingsListCounter < mathSettingsList.size(); mathSettingsListCounter++) {
            String[] split = mathSettingsList.get(mathSettingsListCounter).split(";");


            double tempDouble = 0;



            if (split[1].equalsIgnoreCase("(")) {
                tempDouble = math(recordedEvents, false);
            } else if (split[1].equalsIgnoreCase(")")) {
                break;
            } else {
                tempDouble = getVariable(split, recordedEvents);
            }


            if (split[0].equalsIgnoreCase("+")) {
                returnDouble += tempDouble;

            } else if (split[0].equalsIgnoreCase("-")) {
                returnDouble -= tempDouble;

            } else if (split[0].equalsIgnoreCase("*")) {
                returnDouble *= tempDouble;

            } else if (split[0].equalsIgnoreCase("/")) {
                returnDouble /= tempDouble;

            }


        }

        return returnDouble;

    } // done

    public double getVariable(String[] split, RecordedEvent recordedEvents){


        double tempDouble = 0;

        if(split[1].equalsIgnoreCase("baseScore")){

            tempDouble = currentScoreVariables.baseScore;

        }
        if(split[1].equalsIgnoreCase("bonusOrbVelocity")){

            tempDouble = currentScoreVariables.bonusOrbRotation;

        }
        if(split[1].equalsIgnoreCase("bonusOrbRotation")){

            tempDouble = currentScoreVariables.bonusOrbRotation;

        }
        if(split[1].equalsIgnoreCase("bonusHead")){
            if(recordedEvents.getCaughtWith() == 3) {
                tempDouble = currentScoreVariables.bonusHead;
            } else {
                tempDouble = 0;
            }

        }
        if(split[1].equalsIgnoreCase("bonusFeet")){
            if(recordedEvents.getCaughtWith() == 4 ||
                    recordedEvents.getCaughtWith() == 5) {
                tempDouble = currentScoreVariables.bonusFeet;
            } else {
                tempDouble = 0;
            }

        }
        if(split[1].equalsIgnoreCase("bonusOrbDistance")){

            tempDouble = currentScoreVariables.bonusOrbDistance;

        }
        if(split[1].equalsIgnoreCase("bonusSlider")){
            if(recordedEvents.getTouchType() == 2) {
                tempDouble = currentScoreVariables.bonusSlider;
            } else {
                tempDouble = 0;
            }

        }
        if(split[1].equalsIgnoreCase("bonusSpinner")){
            if(recordedEvents.getTouchType() == 4) {
                tempDouble = currentScoreVariables.bonusSpinner;
            } else {
                tempDouble = 0;
            }

        }
        if(split[1].equalsIgnoreCase("caughtWithDifferentBonus")){
            if(recordedEvents.getCaughtWithDifferent() == 1){
                tempDouble = currentScoreVariables.caughtWithDifferent1;

            } else if(recordedEvents.getCaughtWithDifferent() == 2){
                tempDouble = currentScoreVariables.caughtWithDifferent2;

            } else if(recordedEvents.getCaughtWithDifferent() == 3){
                tempDouble = currentScoreVariables.caughtWithDifferent3;

            } else if(recordedEvents.getCaughtWithDifferent() == 4){
                tempDouble = currentScoreVariables.caughtWithDifferent4;

            } else {
                tempDouble = 0;
            }

        }
        if(split[1].equalsIgnoreCase("caughtWithDifferent")){

                tempDouble = recordedEvents.getCaughtWithDifferent();


        }

        if(split[1].equalsIgnoreCase("noteDuration")){

            tempDouble = recordedEvents.getNoteDuration();

        }
        if(split[1].equalsIgnoreCase("accumulatedTime")){

            tempDouble = recordedEvents.getAccumulatedTime();

        }
        if(split[1].equalsIgnoreCase("rotationBonusFactor")){

            tempDouble = recordedEvents.getRotationBonusFactor();

        }
        if(split[1].equalsIgnoreCase("velocityBonusFactor")){

            tempDouble = recordedEvents.getVelocityBonusFactor();

        }
        if(split[1].equalsIgnoreCase("timingOffset")){

            tempDouble = recordedEvents.getTimingOffset();

        }
        if(split[1].equalsIgnoreCase("distance")){

            tempDouble = recordedEvents.getDistance();

        }
        if(split[1].equalsIgnoreCase("distanceMultiplier")){

            tempDouble = recordedEvents.getDistanceMultiplier();

        }
        if(split[1].equalsIgnoreCase("currentMultiplier")){

            tempDouble = recordedEvents.getCurrentMultiplier();

        }

        return tempDouble;


    }  // done

    public void setCurrentScoreVariables(UiScoreVariablesInput uiScoreVariablesInput) {

        if (calculateScoreBasic) {
            this.mathSettingsList = mathSettingsListClass.basicMathSettingsList;

            currentScoreVariables.baseScore = uiScoreVariablesInput.basicBaseScore;
            currentScoreVariables.bonusOrbVelocity = uiScoreVariablesInput.basicBonusOrbVelocity;
            currentScoreVariables.bonusOrbRotation = uiScoreVariablesInput.basicBonusOrbRotation;
            currentScoreVariables.bonusHead = uiScoreVariablesInput.basicBonusHead;
            currentScoreVariables.bonusFeet = uiScoreVariablesInput.basicBonusFeet;
            currentScoreVariables.bonusOrbDistance = uiScoreVariablesInput.basicBonusOrbDistance;
            currentScoreVariables.bonusSlider = uiScoreVariablesInput.basicBonusSlider;
            currentScoreVariables.bonusSpinner = uiScoreVariablesInput.basicBonusSpinner;
            currentScoreVariables.caughtWithDifferent1 = uiScoreVariablesInput.basicCaughtWithDifferent1;
            currentScoreVariables.caughtWithDifferent2 = uiScoreVariablesInput.basicCaughtWithDifferent2;
            currentScoreVariables.caughtWithDifferent3 = uiScoreVariablesInput.basicCaughtWithDifferent3;
            currentScoreVariables.caughtWithDifferent4 = uiScoreVariablesInput.basicCaughtWithDifferent4;


        } else {
            if (pickupType == 0) {
                this.mathSettingsList = mathSettingsListClass.anyMathSettingList;

                currentScoreVariables.baseScore = uiScoreVariablesInput.advancedAnyBaseScore;
                currentScoreVariables.bonusOrbVelocity = uiScoreVariablesInput.advancedAnyBonusOrbVelocity;
                currentScoreVariables.bonusOrbRotation = uiScoreVariablesInput.advancedAnyBonusOrbRotation;
                currentScoreVariables.bonusHead = uiScoreVariablesInput.advancedAnyBonusHead;
                currentScoreVariables.bonusFeet = uiScoreVariablesInput.advancedAnyBonusFeet;
                currentScoreVariables.bonusOrbDistance = uiScoreVariablesInput.advancedAnyBonusOrbDistance;
                currentScoreVariables.bonusSlider = uiScoreVariablesInput.advancedAnyBonusSlider;
                currentScoreVariables.bonusSpinner = uiScoreVariablesInput.advancedAnyBonusSpinner;
                currentScoreVariables.caughtWithDifferent1 = uiScoreVariablesInput.advancedAnyCaughtWithDifferent1;
                currentScoreVariables.caughtWithDifferent2 = uiScoreVariablesInput.advancedAnyCaughtWithDifferent2;
                currentScoreVariables.caughtWithDifferent3 = uiScoreVariablesInput.advancedAnyCaughtWithDifferent3;
                currentScoreVariables.caughtWithDifferent4 = uiScoreVariablesInput.advancedAnyCaughtWithDifferent4;


            } else if (pickupType == 1) {
                this.mathSettingsList = mathSettingsListClass.handMathSettingList;

                currentScoreVariables.baseScore = uiScoreVariablesInput.advancedHandBaseScore;
                currentScoreVariables.bonusOrbVelocity = uiScoreVariablesInput.advancedHandBonusOrbVelocity;
                currentScoreVariables.bonusOrbRotation = uiScoreVariablesInput.advancedHandBonusOrbRotation;
                currentScoreVariables.bonusHead = uiScoreVariablesInput.advancedHandBonusHead;
                currentScoreVariables.bonusFeet = uiScoreVariablesInput.advancedHandBonusFeet;
                currentScoreVariables.bonusOrbDistance = uiScoreVariablesInput.advancedHandBonusOrbDistance;
                currentScoreVariables.bonusSlider = uiScoreVariablesInput.advancedHandBonusSlider;
                currentScoreVariables.bonusSpinner = uiScoreVariablesInput.advancedHandBonusSpinner;
                currentScoreVariables.caughtWithDifferent1 = uiScoreVariablesInput.advancedHandCaughtWithDifferent1;
                currentScoreVariables.caughtWithDifferent2 = uiScoreVariablesInput.advancedHandCaughtWithDifferent2;
                currentScoreVariables.caughtWithDifferent3 = uiScoreVariablesInput.advancedHandCaughtWithDifferent3;
                currentScoreVariables.caughtWithDifferent4 = uiScoreVariablesInput.advancedHandCaughtWithDifferent4;

            } else if (pickupType == 2) {
                this.mathSettingsList = mathSettingsListClass.headMathSettingList;

                currentScoreVariables.baseScore = uiScoreVariablesInput.advancedHeadBaseScore;
                currentScoreVariables.bonusOrbVelocity = uiScoreVariablesInput.advancedHeadBonusOrbVelocity;
                currentScoreVariables.bonusOrbRotation = uiScoreVariablesInput.advancedHeadBonusOrbRotation;
                currentScoreVariables.bonusHead = uiScoreVariablesInput.advancedHeadBonusHead;
                currentScoreVariables.bonusFeet = uiScoreVariablesInput.advancedHeadBonusFeet;
                currentScoreVariables.bonusOrbDistance = uiScoreVariablesInput.advancedHeadBonusOrbDistance;
                currentScoreVariables.bonusSlider = uiScoreVariablesInput.advancedHeadBonusSlider;
                currentScoreVariables.bonusSpinner = uiScoreVariablesInput.advancedHeadBonusSpinner;
                currentScoreVariables.caughtWithDifferent1 = uiScoreVariablesInput.advancedHeadCaughtWithDifferent1;
                currentScoreVariables.caughtWithDifferent2 = uiScoreVariablesInput.advancedHeadCaughtWithDifferent2;
                currentScoreVariables.caughtWithDifferent3 = uiScoreVariablesInput.advancedHeadCaughtWithDifferent3;
                currentScoreVariables.caughtWithDifferent4 = uiScoreVariablesInput.advancedHeadCaughtWithDifferent4;

            } else if (pickupType == 3) {
                this.mathSettingsList = mathSettingsListClass.footMathSettingList;

                currentScoreVariables.baseScore = uiScoreVariablesInput.advancedFootBaseScore;
                currentScoreVariables.bonusOrbVelocity = uiScoreVariablesInput.advancedFootBonusOrbVelocity;
                currentScoreVariables.bonusOrbRotation = uiScoreVariablesInput.advancedFootBonusOrbRotation;
                currentScoreVariables.bonusHead = uiScoreVariablesInput.advancedFootBonusHead;
                currentScoreVariables.bonusFeet = uiScoreVariablesInput.advancedFootBonusFeet;
                currentScoreVariables.bonusOrbDistance = uiScoreVariablesInput.advancedFootBonusOrbDistance;
                currentScoreVariables.bonusSlider = uiScoreVariablesInput.advancedFootBonusSlider;
                currentScoreVariables.bonusSpinner = uiScoreVariablesInput.advancedFootBonusSpinner;
                currentScoreVariables.caughtWithDifferent1 = uiScoreVariablesInput.advancedFootCaughtWithDifferent1;
                currentScoreVariables.caughtWithDifferent2 = uiScoreVariablesInput.advancedFootCaughtWithDifferent2;
                currentScoreVariables.caughtWithDifferent3 = uiScoreVariablesInput.advancedFootCaughtWithDifferent3;
                currentScoreVariables.caughtWithDifferent4 = uiScoreVariablesInput.advancedFootCaughtWithDifferent4;

            }
        }


    } // done
}
