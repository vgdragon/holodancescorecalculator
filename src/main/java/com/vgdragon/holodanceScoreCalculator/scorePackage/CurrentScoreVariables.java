package com.vgdragon.holodanceScoreCalculator.scorePackage;

public class CurrentScoreVariables {

    public int baseScore;
    public int bonusOrbVelocity;
    public int bonusOrbRotation;
    public int bonusHead;
    public int bonusFeet;
    public int bonusOrbDistance;
    public int bonusSlider;
    public int bonusSpinner;
    public int caughtWithDifferent1;
    public int caughtWithDifferent2;
    public int caughtWithDifferent3;
    public int caughtWithDifferent4;

}
