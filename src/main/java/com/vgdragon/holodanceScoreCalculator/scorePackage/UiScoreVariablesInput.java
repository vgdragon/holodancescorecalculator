package com.vgdragon.holodanceScoreCalculator.scorePackage;

public class UiScoreVariablesInput {

    //Any

    public int advancedAnyBaseScore;
    public int advancedAnyBonusOrbVelocity;
    public int advancedAnyBonusOrbRotation;
    public int advancedAnyBonusHead;
    public int advancedAnyBonusFeet;
    public int advancedAnyBonusOrbDistance;
    public int advancedAnyBonusSlider;
    public int advancedAnyBonusSpinner;
    public int advancedAnyCaughtWithDifferent1;
    public int advancedAnyCaughtWithDifferent2;
    public int advancedAnyCaughtWithDifferent3;
    public int advancedAnyCaughtWithDifferent4;

    //Hand

    public int advancedHandBaseScore;
    public int advancedHandBonusOrbVelocity;
    public int advancedHandBonusOrbRotation;
    public int advancedHandBonusHead;
    public int advancedHandBonusFeet;
    public int advancedHandBonusOrbDistance;
    public int advancedHandBonusSlider;
    public int advancedHandBonusSpinner;
    public int advancedHandCaughtWithDifferent1;
    public int advancedHandCaughtWithDifferent2;
    public int advancedHandCaughtWithDifferent3;
    public int advancedHandCaughtWithDifferent4;

    //Head

    public int advancedHeadBaseScore;
    public int advancedHeadBonusOrbVelocity;
    public int advancedHeadBonusOrbRotation;
    public int advancedHeadBonusHead;
    public int advancedHeadBonusFeet;
    public int advancedHeadBonusOrbDistance;
    public int advancedHeadBonusSlider;
    public int advancedHeadBonusSpinner;
    public int advancedHeadCaughtWithDifferent1;
    public int advancedHeadCaughtWithDifferent2;
    public int advancedHeadCaughtWithDifferent3;
    public int advancedHeadCaughtWithDifferent4;

    //Foot

    public int advancedFootBaseScore;
    public int advancedFootBonusOrbVelocity;
    public int advancedFootBonusOrbRotation;
    public int advancedFootBonusHead;
    public int advancedFootBonusFeet;
    public int advancedFootBonusOrbDistance;
    public int advancedFootBonusSlider;
    public int advancedFootBonusSpinner;
    public int advancedFootCaughtWithDifferent1;
    public int advancedFootCaughtWithDifferent2;
    public int advancedFootCaughtWithDifferent3;
    public int advancedFootCaughtWithDifferent4;

    // basic

    public int basicBaseScore;
    public int basicBonusOrbVelocity;
    public int basicBonusOrbRotation;
    public int basicBonusHead;
    public int basicBonusFeet;
    public int basicBonusOrbDistance;
    public int basicBonusSlider;
    public int basicBonusSpinner;
    public int basicCaughtWithDifferent1;
    public int basicCaughtWithDifferent2;
    public int basicCaughtWithDifferent3;
    public int basicCaughtWithDifferent4;


    //baseScore
    //bonusOrbVelocity
    //bonusOrbRotation
    //bonusHead
    //bonusFeet
    //bonusOrbDistance
    //bonusSlider
    //bonusSpinner
    //caughtWithDifferent1
    //caughtWithDifferent2
    //caughtWithDifferent3
    //caughtWithDifferent4
    //noteDuration
    //accumulatedTime
    //rotationBonusFactor
    //velocityBonusFactor
    //timingOffset
    //distance
    //distanceMultiplier




}
