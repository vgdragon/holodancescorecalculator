package com.vgdragon.holodanceScoreCalculator.Graphics.Stats;

import java.util.ArrayList;
import java.util.List;

public class BeBetterStats {

    private int orbCount = 0;
    private int missedOrbCount = 0;

    private int sliderCount = 0;
    private int missedSliderCount = 0;
    private int maxPointsSliderCount = 0;
    private List<Integer> sliderPointPercentLost = new ArrayList<>();
    private int sliderNotCompletedPercent = 0;

    private int spinnerCount = 0;
    private int missedSpinnerCount = 0;
    private int maxPointsSpinnerCount = 0;
    private List<Integer> spinnerPointPercentLost = new ArrayList<>();
    private int spinnerNotCompletedPercent = 0;

    public int getOrbCount() {
        return orbCount;
    }

    public void setOrbCount(int orbCount) {
        this.orbCount = orbCount;
    }

    public int getMissedOrbCount() {
        return missedOrbCount;
    }

    public void setMissedOrbCount(int missedOrbCount) {
        this.missedOrbCount = missedOrbCount;
    }

    public int getSliderCount() {
        return sliderCount;
    }

    public void setSliderCount(int sliderCount) {
        this.sliderCount = sliderCount;
    }

    public int getMissedSliderCount() {
        return missedSliderCount;
    }

    public void setMissedSliderCount(int missedSliderCount) {
        this.missedSliderCount = missedSliderCount;
    }

    public int getMaxPointsSliderCount() {
        return maxPointsSliderCount;
    }

    public void setMaxPointsSliderCount(int maxPointsSliderCount) {
        this.maxPointsSliderCount = maxPointsSliderCount;
    }

    public List<Integer> getSliderPointPercentLost() {
        return sliderPointPercentLost;
    }

    public void setSliderPointPercentLost(List<Integer> sliderPointPercentLost) {
        this.sliderPointPercentLost = sliderPointPercentLost;
    }

    public int getSliderNotCompletedPercent() {
        return sliderNotCompletedPercent;
    }

    public void setSliderNotCompletedPercent(int sliderNotCompletedPercent) {
        this.sliderNotCompletedPercent = sliderNotCompletedPercent;
    }

    public int getSpinnerCount() {
        return spinnerCount;
    }

    public void setSpinnerCount(int spinnerCount) {
        this.spinnerCount = spinnerCount;
    }

    public int getMissedSpinnerCount() {
        return missedSpinnerCount;
    }

    public void setMissedSpinnerCount(int missedSpinnerCount) {
        this.missedSpinnerCount = missedSpinnerCount;
    }

    public int getMaxPointsSpinnerCount() {
        return maxPointsSpinnerCount;
    }

    public void setMaxPointsSpinnerCount(int maxPointsSpinnerCount) {
        this.maxPointsSpinnerCount = maxPointsSpinnerCount;
    }

    public List<Integer> getSpinnerPointPercentLost() {
        return spinnerPointPercentLost;
    }

    public void setSpinnerPointPercentLost(List<Integer> spinnerPointPercentLost) {
        this.spinnerPointPercentLost = spinnerPointPercentLost;
    }

    public int getSpinnerNotCompletedPercent() {
        return spinnerNotCompletedPercent;
    }

    public void setSpinnerNotCompletedPercent(int spinnerNotCompletedPercent) {
        this.spinnerNotCompletedPercent = spinnerNotCompletedPercent;
    }
}
