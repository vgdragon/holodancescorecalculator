package com.vgdragon.holodanceScoreCalculator.Graphics;

import com.vgdragon.holodanceScoreCalculator.Graphics.Stats.BeBetterStats;

import javax.swing.*;
import java.awt.*;

public class StatsGraphic extends JPanel{

    private BeBetterStats beBetterStats;



    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if(beBetterStats != null){
        this.setBackground(Color.WHITE);

        graphics.setColor(Color.RED);
        Double temp = ((double) 100 / beBetterStats.getOrbCount()) * (beBetterStats.getOrbCount() - beBetterStats.getMissedOrbCount());
        graphics.fillRect(25, 25, temp.intValue() * 3, 10);

        graphics.setColor(Color.BLUE);
        temp = ((double) 100 / beBetterStats.getSliderCount()) * (beBetterStats.getSliderCount() - beBetterStats.getMissedSliderCount());
        graphics.fillRect(25, 35, temp.intValue() * 3, 10);

        graphics.setColor(Color.YELLOW);
        temp = ((double) 100 / beBetterStats.getSpinnerCount()) * (beBetterStats.getSpinnerCount() - beBetterStats.getMissedSpinnerCount());
        graphics.fillRect(25, 45, temp.intValue() * 3, 10);

        graphics.setColor(Color.GREEN);
        graphics.fillRect(25, 55, (beBetterStats.getSliderNotCompletedPercent() * 3), 10);

        graphics.setColor(Color.MAGENTA);
        graphics.fillRect(25, 65, (beBetterStats.getSpinnerNotCompletedPercent() * 3), 10);

        graphics.setColor(Color.BLACK);
        graphics.fillRect(25, 80, 2, 12);
        graphics.fillRect(25 + (10 * 3), 83, 2, 4);
        graphics.fillRect(25 + (20 * 3), 83, 2, 4);
        graphics.fillRect(25 + (30 * 3), 83, 2, 4);
        graphics.fillRect(25 + (40 * 3), 83, 2, 4);
        graphics.fillRect(25 + (50 * 3), 83, 2, 4);
        graphics.fillRect(25 + (60 * 3), 83, 2, 4);
        graphics.fillRect(25 + (70 * 3), 83, 2, 4);
        graphics.fillRect(25 + (80 * 3), 83, 2, 4);
        graphics.fillRect(25 + (90 * 3), 83, 2, 4);
        graphics.fillRect(25 + (100 * 3), 83, 2, 4);

        graphics.fillRect(25, 85, 300, 2);
    }



    }

    public void setBeBetterStats(BeBetterStats beBetterStats){
        this.beBetterStats = beBetterStats;
        repaint();
    }

}
